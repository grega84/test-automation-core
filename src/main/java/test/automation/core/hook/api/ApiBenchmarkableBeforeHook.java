package test.automation.core.hook.api;

import java.util.List;

import cucumber.api.Scenario;
import cucumber.api.java.Before;
import test.automation.core.benchmark.ApiBenchmarkableContext;
import test.automation.core.benchmark.BenchmarkableContext;
import test.automation.core.hook.common.BenchmarkableBeforeHook;

public class ApiBenchmarkableBeforeHook extends BenchmarkableBeforeHook{
	
	@Before(order=2)
	public void runHook(Scenario scenario) {
		
		super.runHook(scenario);
	}
	
	@Override
	protected BenchmarkableContext buildBenchmarkableContext(List<String> annotations, String testRunId) {
				
		return new ApiBenchmarkableContext(annotations,testRunId);
		
	}		
	
}
