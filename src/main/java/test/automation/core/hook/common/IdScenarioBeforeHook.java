package test.automation.core.hook.common;

import cucumber.api.Scenario;
import cucumber.api.java.Before;
import test.automation.core.UIUtils;

/**
 * This hook gets the Scenario ID and store it in a properties
 *
 */
public class IdScenarioBeforeHook {
	@Before(order=1)
	public void runHook(Scenario scenario){
		System.setProperty(UIUtils.SCENARIO_ID, Integer.toString(scenario.getId().hashCode()));
	}
}
