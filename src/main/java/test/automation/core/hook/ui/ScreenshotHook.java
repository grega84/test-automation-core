package test.automation.core.hook.ui;

import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.WebDriver;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import test.automation.core.UIUtils;


public class ScreenshotHook {
	
			
	@After(order=11111)
	public void runHook(Scenario scenario){
		if (scenario.isFailed()){
			//takes a screenshot for failed scenario
			takeScreenshotOnFailure(scenario);
		}else{
			//embeds the screenshot taken if any
			embedScreenshot(scenario);
		}
	}
	
	private void takeScreenshotOnFailure(Scenario scenario){
		try {
			WebDriver currentDriver = UIUtils.ui().getCurrentDriver();
			String fileName = UIUtils.ui().takeScreenshot(currentDriver);
			scenario.write("<img src=\"embeddings\\" + fileName + " \" >");
    	} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void embedScreenshot(Scenario scenario){
		int scenarioID = scenario.getId().hashCode();
		String reportImagesDir = System.getProperty(UIUtils.REPORT_IMAGES_DIR);
		try {
			List<Path> listImagesFile = Files.walk(Paths.get(reportImagesDir))
			 .filter(Files::isRegularFile)
			 .filter(f -> StringUtils.contains(f.getFileName().toString(), Integer.toString(scenarioID)))
			 .collect(toList());
			for (Path path : listImagesFile) {
				scenario.write("<img src=\"embeddings\\" + path.toFile().getName() + " \" >");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
}
