package test.automation.core;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.springframework.kafka.core.KafkaTemplate;

import test.automation.core.kafka.AvroSerializer;
import test.automation.core.kafka.KafkaConstants;
import test.automation.core.kafka.KafkaPropertiesReader;
import test.automation.core.kafka.KafkaSerializer;

/**
 * This class provides utilities method to get pre-configured KafkaTemplate<K, V>
 * instances using externalized properties.
 * 
 */
public class KafkaMessageUtils<K, V> {
    
	@SuppressWarnings("rawtypes")
	private static KafkaMessageUtils instance;
	// create a specific KafkaTemplate for every different server
	private Map<String, KafkaTemplate<K, V>> context;

	private KafkaSerializer<K, V> kafkaSerializer;

	private KafkaTemplate<K, V> kafkaTemplate;

	private KafkaMessageUtils() {

		context = new HashMap<String, KafkaTemplate<K, V>>();
		Map<String, Properties> propertiesBundles = KafkaPropertiesReader
				.read();

		for (Entry<String, Properties> propertyBundle : propertiesBundles
				.entrySet()) {
			registerKafkaTemplate(propertyBundle.getKey(),
					propertyBundle.getValue());

		}

	}
	
	/**
	 * singleton factory method
	 * 
	 * @return single instance of KafkaMessageUtils
	 */
	@SuppressWarnings("unchecked")
	public static <K, V> KafkaMessageUtils<K, V> getInstance() {
		if (instance != null) {
			return instance;
		}

		synchronized (KafkaMessageUtils.class) {
			if (instance != null) {
				return instance;
			}

			instance = new KafkaMessageUtils<K, V>();
			return instance;

		}
	}
	
	/**
	 * returning the kafka template for default kafka server. It was always
	 * guaranteed to have a kafka template for default server
	 */
	public KafkaTemplate<K, V> template() {
		if (!context.containsKey("default"))
			throw new IllegalArgumentException("resource default is not set");

		return context.get(KafkaConstants.DEFAULT_API_PROPERTIES_BUNDLE_KEY);
	}

	public KafkaTemplate<K, V> template(String resourceName) {
		if (!context.containsKey(resourceName))
			throw new IllegalArgumentException("resource " + resourceName
					+ " is not set");

		return context.get(resourceName);
	}

	/**
	 * Create a KafkaTemplate<K, V>. Currently only Avro serialization is
	 * supported.
	 * 
	 * @param resourceName
	 * @param properties
	 * @return
	 */
	private void registerKafkaTemplate(String resourceName,
			Properties properties) {

		if (context.containsKey(resourceName)) {
			throw new IllegalArgumentException("resource " + resourceName
					+ " already set in the context. Override is not allowed");
		}

		if (StringUtils.equalsIgnoreCase(
				properties.getProperty(KafkaConstants.SERIALIZATION_TYPE),
				KafkaConstants.AVRO_SERIALIZATION)) {

			kafkaSerializer = new AvroSerializer<K, V>();

		} else {

			throw new IllegalArgumentException(
					"Unsupported serialization type!");

		}

		kafkaTemplate = new KafkaTemplate<>(
				kafkaSerializer.producerFactory(properties));

		context.put(resourceName, kafkaTemplate);

	}
}