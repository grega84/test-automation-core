package test.automation.core;

import java.io.StringReader;
import java.time.Duration;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import test.automation.core.UIUtils.SCROLL_DIRECTION;

public class MobileUtils 
{
	
	public void swipe(MobileDriver driver, UIUtils.SCROLL_DIRECTION direction, long duration) {
	    Dimension size = driver.manage().window().getSize();

	    int startX = 0;
	    int endX = 0;
	    int startY = 0;
	    int endY = 0;

	    switch (direction) {
	        case RIGHT:
	            startY = (int) (size.height / 2);
	            startX = (int) (size.width * 0.90);
	            endX = (int) (size.width * 0.05);
	            
	            new TouchAction(driver)
	                    .press(PointOption.point(startX, startY))
	                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration)))
	                    .moveTo(PointOption.point(endX, startY))
	                    .release()
	                    .perform();
	            break;

	        case LEFT:
	            startY = (int) (size.height / 2);
	            startX = (int) (size.width * 0.05);
	            endX = (int) (size.width * 0.90);
	           new TouchAction(driver)
	                    .press(PointOption.point(startX, startY))
	                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration)))
	                    .moveTo(PointOption.point(endX, startY))
	                    .release()
	                    .perform();

	            break;

	        case UP:
	            endY = (int) (size.height * 0.70);
	            startY = (int) (size.height * 0.30);
	            startX = (size.width / 2);
	            new TouchAction(driver)
	                    .press(PointOption.point(startX, startY))
	                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration)))
	                    .moveTo(PointOption.point(startX, endY))
	                    .release()
	                    .perform();
	            break;


	        case DOWN:
	            startY = (int) (size.height * 0.70);
	            endY = (int) (size.height * 0.30);
	            startX = (size.width / 2);
	           new TouchAction(driver)
	                    .press(PointOption.point(startX, startY))
	                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration)))
	                    .moveTo(PointOption.point(startX, endY))
	                    .release()
	                    .perform();

	            break;

	    }
	}
	
	public void swipeToElementIOS(AppiumDriver<?> driver, UIUtils.SCROLL_DIRECTION direction, float f, float g, float h, float i, int duration,
			String elementXpath, int k) 
	{
		for(int j=0;j<k;j++)
		{
			try
			{
				
				UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath)),1);
				return;
			}
			catch(Exception err)
			{
				
			}
			
			swipe(driver, direction,f,g,h,i, duration);
		}
	}
	
	public void tapWebViewElement(AppiumDriver appDriver, WebElement el)
	{
	    try {
	        appDriver.hideKeyboard();
	    }catch (Exception e){
	        // System.out.println("No Keyboard up " + e.getMessage());
	    }
	    
	    Point pt = el.getLocation();
	    Dimension size = el.getSize();
	    int x = pt.getX() +(size.getWidth() / 2);
	    int y = pt.getY() +  (size.getHeight() / 2);
	   
	 JavascriptExecutor js = (JavascriptExecutor) appDriver;
	     
	 int screenWebViewWidth = ((Long) js.executeScript("return window.innerWidth || document.body.clientWidth")).intValue();
	    
	  int screenWebViewHeight = ((Long) js.executeScript("return window.innerHeight || document.body.clientHeight")).intValue();

	    //Scroll If Necessary
	    if(y > screenWebViewHeight){
	        js.executeScript("arguments[0].scrollIntoView(true);", el);
	        try{Thread.sleep(500);}catch (InterruptedException e){}
	        pt = el.getLocation();
	        size = el.getSize();
	        x = pt.getX() +(size.getWidth() / 2);
	        y = pt.getY() +  (size.getHeight() / 2);
	    }
	    Set<String> contextNames = ((AppiumDriver) appDriver).getContextHandles();
	    
	    for (String contextName : contextNames) 
	    {
	            if (contextName.contains("NATIVE")) {
	                ((AppiumDriver<MobileElement>) appDriver).context(contextName);
	                System.out.println(" Switching to: " + contextName);

	            }

	    }
	    
	    double screenWidth = appDriver.manage().window().getSize().getWidth();
	    double screenHeight = appDriver.manage().window().getSize().getHeight();

	    double elementNativeViewX = (x * screenWidth) / screenWebViewWidth;
	    double elementNativeViewY = (y * screenHeight) / screenWebViewHeight;
	    
	    System.out.println( " Real Tap X:"+elementNativeViewX + ", Real Tap Y:"+elementNativeViewY);
	    try{Thread.sleep(1000);}catch(Exception e){};
	    
	    new TouchAction((MobileDriver) appDriver)
	    .tap(PointOption.point((int)elementNativeViewX,(int)elementNativeViewY))
	    .perform();

	    contextNames = ((AppiumDriver) appDriver).getContextHandles();
	    for (String contextName : contextNames) 
	    {
	    	if (contextName.contains("CHROMIUM")) {
                ((AppiumDriver<MobileElement>) appDriver).context(contextName);
                System.out.println(" Switching to: " + contextName);

            }
	    }
	}
	
	public void tapOnWebView(AppiumDriver appDriver,int x,int y) 
	{
		try {
	        appDriver.hideKeyboard();
	    }catch (Exception e){
	        // System.out.println("No Keyboard up " + e.getMessage());
	    }
	    
		JavascriptExecutor js = (JavascriptExecutor) appDriver;
	     
		int screenWebViewWidth = ((Long) js.executeScript("return window.innerWidth || document.body.clientWidth")).intValue();
		int screenWebViewHeight = ((Long) js.executeScript("return window.innerHeight || document.body.clientHeight")).intValue();

	    Set<String> contextNames = ((AppiumDriver) appDriver).getContextHandles();
	    
	    for (String contextName : contextNames) 
	    {
	            if (contextName.contains("NATIVE")) {
	                ((AppiumDriver<MobileElement>) appDriver).context(contextName);
	                System.out.println(" Switching to: " + contextName);

	            }

	    }
	    
	    double screenWidth = appDriver.manage().window().getSize().getWidth();
	    double screenHeight = appDriver.manage().window().getSize().getHeight();

	    double elementNativeViewX = (x * screenWidth) / screenWebViewWidth;
	    double elementNativeViewY = (y * screenHeight) / screenWebViewHeight;
	    
	    System.out.println( " Real Tap X:"+elementNativeViewX + ", Real Tap Y:"+elementNativeViewY);
	    try{Thread.sleep(1000);}catch(Exception e){};
	    
	    new TouchAction((MobileDriver) appDriver)
	    .tap(PointOption.point((int)elementNativeViewX,(int)elementNativeViewY))
	    .perform();

	    contextNames = ((AppiumDriver) appDriver).getContextHandles();
	    for (String contextName : contextNames) 
	    {
	    	if (contextName.contains("CHROMIUM")) {
                ((AppiumDriver<MobileElement>) appDriver).context(contextName);
                System.out.println(" Switching to: " + contextName);

            }
	    }
	}
	
	public void swipeToElement(MobileDriver driver, UIUtils.SCROLL_DIRECTION direction, long duration,String element2FindInPage,int repeats)
	{
		for(int i=0;i<repeats;i++)
		{
			try
			{
				Thread.sleep(TimeUnit.SECONDS.toMillis(1));
				
				String pageSource=driver.getPageSource();
				Document page=null;
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
				DocumentBuilder builder;  
				try {
					builder = factory.newDocumentBuilder();  
					page = builder.parse(new InputSource(new StringReader(pageSource)));
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				XPath xPath = XPathFactory.newInstance().newXPath();
				NodeList n=(NodeList) xPath.compile(element2FindInPage).evaluate(page,XPathConstants.NODESET);
				
				if(n != null && n.getLength() > 0) return;
				
				swipe(driver, direction, duration);
			}
			catch(Exception err)
			{
				
			}
		}
	}
	
	public void selectElementFromIOSPickerWeel(MobileDriver driver,String list, String element,String element2Find) 
	{
		List<WebElement> PicherWeelList=driver.findElements(By.xpath(list));
		
		for(int i=0;i<PicherWeelList.size();i++)
		{
			String txt=((WebElement) driver.findElements(By.xpath(element)).get(0)).getText().trim().toLowerCase();
			
			if(txt.equals(element2Find.toLowerCase()))
			{
				driver.findElement(By.xpath("//*[@text='Fine']")).click();
				return;
			}
			
			swipe( driver, UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.8f, 0.5f, 0.75f, 1000);
		}
	}
	
	public void swipe(MobileDriver driver, UIUtils.SCROLL_DIRECTION direction,float fromDx,float fromDy,float toDx,float toDy, long duration)
	{
		Dimension size = driver.manage().window().getSize();

	    int startX = 0;
	    int endX = 0;
	    int startY = 0;
	    int endY = 0;

	    switch (direction) {
	        case RIGHT:
	            startY = (int) (size.height * fromDy);
	            startX = (int) (size.width * fromDx);
	            endX = (int) (size.width * toDx);
	           new TouchAction(driver)
	                    .press(PointOption.point(startX, startY))
	                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration)))
	                    .moveTo(PointOption.point(endX, startY))
	                    .release()
	                    .perform();
	            break;

	        case LEFT:
	            startY = (int) (size.height * fromDy);
	            startX = (int) (size.width * fromDx);
	            endX = (int) (size.width * toDx);
	            new TouchAction(driver)
	                    .press(PointOption.point(startX, startY))
	                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration)))
	                    .moveTo(PointOption.point(endX, startY))
	                    .release()
	                    .perform();

	            break;

	        case UP:
	            endY = (int) (size.height * toDy);
	            startY = (int) (size.height * fromDy);
	            startX = (int) (size.width * fromDx);
	           new TouchAction(driver)
	                    .press(PointOption.point(startX, startY))
	                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration)))
	                    .moveTo(PointOption.point(startX, endY))
	                    .release()
	                    .perform();
	            break;


	        case DOWN:
	            startY = (int) (size.height * fromDy);
	            endY = (int) (size.height * toDy);
	            startX = (int) (size.width * fromDx);
	            new TouchAction(driver)
	                    .press(PointOption.point(startX, startY))
	                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration)))
	                    .moveTo(PointOption.point(startX, endY))
	                    .release()
	                    .perform();

	            break;

	    }
	}
}
