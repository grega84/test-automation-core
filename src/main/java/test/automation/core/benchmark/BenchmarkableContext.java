package test.automation.core.benchmark;

import java.util.Collections;
import java.util.List;

public abstract class BenchmarkableContext {
	
	private final String testRunId;
	private final List<BenchmarkableAnnotationObject> benchmarkableObjectList;

	public BenchmarkableContext(List<String> annotations, String testRunId){

		this.benchmarkableObjectList=convertAnnotation(annotations);
		this.testRunId=testRunId;
	}		
		
	protected abstract List<BenchmarkableAnnotationObject> convertAnnotation(List<String> annotation);

	public List<BenchmarkableAnnotationObject> getBenchmarkableObjectList() {
		return Collections.unmodifiableList(benchmarkableObjectList);
	}

	public String getTestRunId() {
		return testRunId;
	}
	
}
