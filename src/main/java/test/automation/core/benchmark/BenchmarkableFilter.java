package test.automation.core.benchmark;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import io.restassured.filter.Filter;
import io.restassured.filter.FilterContext;
import io.restassured.http.Header;
import io.restassured.response.Response;
import io.restassured.specification.FilterableRequestSpecification;
import io.restassured.specification.FilterableResponseSpecification;
import test.automation.core.AutomationThreadLocal;

public class BenchmarkableFilter implements Filter {

	@Override
	public Response filter(FilterableRequestSpecification requestSpecification,
			FilterableResponseSpecification responseSpecification, FilterContext ctx) {

		BenchmarkableContext context = AutomationThreadLocal.get();
		if (context != null) {
			List<BenchmarkableAnnotationObject> benchmarkableObjectList = context.getBenchmarkableObjectList();

			
			for (BenchmarkableAnnotationObject benchmarkableObject : benchmarkableObjectList) {
				String path = benchmarkableObject.getPath();
				// replace '.' with '/'
				
				path = StringUtils.replace(path, ".", "/");
				if (StringUtils.contains(requestSpecification.getURI(), path) && StringUtils
						.equalsIgnoreCase(benchmarkableObject.getMethod(), requestSpecification.getMethod())) {
					
					Header dynatraceHeader = new Header("X-dynaTrace",
							"TN=" + benchmarkableObject.getName() + ";TR="+context.getTestRunId());
					requestSpecification.header(dynatraceHeader);
				}
			}
		}
		return ctx.next(requestSpecification, responseSpecification);
	}

}
