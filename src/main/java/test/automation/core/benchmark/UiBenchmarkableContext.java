package test.automation.core.benchmark;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;

public class UiBenchmarkableContext extends BenchmarkableContext{
	
	private WebDriver driver;
	
	public UiBenchmarkableContext(List<String> annotations, String testRunId){
		
		super(annotations,testRunId);
	}		
	
	@Override
	protected List<BenchmarkableAnnotationObject> convertAnnotation(
			List<String> annotations) {
        
		List<BenchmarkableAnnotationObject> objectList = new ArrayList<>();
		
		BenchmarkableAnnotationObject obj;
		for (String annotation: annotations) {
			
			if (annotations.size()>1){
				throw new IllegalArgumentException("More than one benchmarkable annotation");
		   }
			
			if (StringUtils.isNotBlank(annotation)){
				String payload = StringUtils.substringBetween(annotation, "@benchmarkable(", ")");
				String[] tokens = StringUtils.split(payload, ",");
				obj = new BenchmarkableAnnotationObject();
				for (String token : tokens) {
					String[] split = token.split("=");
				if (split.length==2){
					String key = split[0].trim();
					String value = split[1].trim().replaceAll("^\"|\"$", "");
					if (StringUtils.equals(key, "name")){
						obj.setName(value);
					}
				  }
				}
				
				if(StringUtils.isEmpty(obj.getName())){
					throw new IllegalArgumentException("Missed name property inside benchmarkable annotation"); 
				}
				
				objectList.add(obj);
				
			  }
		}	
				
		return objectList;
		
	}	
	
	public void setDriver(WebDriver driver){
		this.driver=driver;
	}
	
	public WebDriver getDriver(){
		return driver;
	}
	
}
