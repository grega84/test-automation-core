package test.automation.core.benchmark;

import java.io.Serializable;

/**
 * This class contains the information about the service to monitor
 *
 */
public class BenchmarkableAnnotationObject implements Serializable {
		
	private String name = null;
	private String path = null;
	private String method = null;
		
	public String getName() {
		return name;
	}
	public String getPath() {
		return path;
	}
	public String getMethod() {
		return method;
	}
	public void setName(String aggregationField) {
		this.name = aggregationField;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	
	@Override
	public String toString() {
		StringBuilder sb= new StringBuilder();
		sb.append("name").append("=").append(name).append("\r\n");
		sb.append("path").append("=").append(path).append("\r\n");
		sb.append("method").append("=").append(method).append("\r\n");
		return sb.toString();
	}
	

}
