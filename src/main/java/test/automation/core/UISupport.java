package test.automation.core;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import io.appium.java_client.android.AndroidDriver;

public final class UISupport {

	public static BufferedImage captureScreenshoot() throws Exception {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		BufferedImage image = new Robot().createScreenCapture(new Rectangle(screenSize));

		// scaling 1/2 aspect ratio
		int newW = (int) screenSize.getWidth() / 2;
		int newH = (int) screenSize.getHeight() / 2;

		return scale(image, newW, newH);

	}

	public static BufferedImage scale(BufferedImage src, int w, int h) {
		BufferedImage img = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		int x, y;
		int ww = src.getWidth();
		int hh = src.getHeight();
		for (x = 0; x < w; x++) {
			for (y = 0; y < h; y++) {
				int col = src.getRGB(x * ww / w, y * hh / h);
				img.setRGB(x, y, col);
			}
		}
		return img;
	}

	public static File captureScreenshoot(AndroidDriver driver) {
		String currentContext = driver.getContext();
		Boolean contextChanged = Boolean.FALSE;
		if (!StringUtils.equalsIgnoreCase("NATIVE_APP", currentContext)){
			driver.context("NATIVE_APP");
			contextChanged = Boolean.TRUE;
		}
		File screenshotAsFile = driver.getScreenshotAs(OutputType.FILE);
		//switch context to original one
		if (contextChanged){
			driver.context(currentContext);
		}
		return screenshotAsFile;
		
	}
	
	/**
	 * Take a Screenhoot for using WebDriverApi
	 * @param driver
	 * @return
	 */
	public static File captureScreenshoot(WebDriver driver){
		if ( driver == null ){
			throw new IllegalArgumentException("Driver is null!");
		}
		return ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	}

}
