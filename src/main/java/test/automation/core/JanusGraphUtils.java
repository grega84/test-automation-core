package test.automation.core;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.lang.StringUtils;

import test.automation.core.graph.JANUSGraphCluster;
import test.automation.core.graph.JANUSGraphClusterFactory;
import test.automation.core.graph.JANUSGraphSession;
import test.automation.core.graph.YAMLConfiguration;
import test.automation.core.properties.FileMultiPropertiesLoaderImpl;
import test.automation.core.properties.MultiPropertiesLoader;

/**
 * Wrap the JANUSGraphSession in order to enable queries on JanusGraph
 * distributed graph database.
 * 
 */
public class JanusGraphUtils {

	private static JanusGraphUtils instance;
	private Map<String, JANUSGraphSession> context;

	private static final String DEFAULT_JANUS_GRAPH_PROPERTIES_BUNDLE_KEY = "default";

	private static final String JANUS_GRAPH_SERVER_HOST = "janus.graph.server.host";
	private static final String JANUS_GRAPH_SERVER_PORT = "janus.graph.server.port";
	private static final String JANUS_GRAPH_NAME = "janus.graph.name";
	private static final String JANUS_GRAPH_USERNAME = "janus.graph.username";
	private static final String JANUS_GRAPH_PASSWORD = "janus.graph.password";
	private static final String JANUS_REMOTE_OBJECTS_YAML_PATH = "janus.remote.objects.yaml.path";

	private JanusGraphUtils() {
		context = new HashMap<String, JANUSGraphSession>();
		MultiPropertiesLoader multiPropertiesLoader = new FileMultiPropertiesLoaderImpl(
				(file) -> file.getName().split("-")[0]);

		Map<String, Properties> propertiesBundles = multiPropertiesLoader.loadAndCheck(
				(d, name) -> name.endsWith("-janusgraph.properties"), JANUS_GRAPH_SERVER_HOST, JANUS_GRAPH_SERVER_PORT,
				JANUS_GRAPH_NAME, JANUS_GRAPH_USERNAME);

		if (!propertiesBundles.containsKey(DEFAULT_JANUS_GRAPH_PROPERTIES_BUNDLE_KEY))
			throw new IllegalArgumentException("No default-janusgraph.properties found in the "
					+ ((FileMultiPropertiesLoaderImpl) multiPropertiesLoader).getPath());

		for (Entry<String, Properties> propertyBundle : propertiesBundles.entrySet()) {
			registerJanusGraphSession(propertyBundle.getKey(), propertyBundle.getValue());

		}

	}

	/**
	 * singleton factory method
	 * 
	 * @return single instance of JanusGraphUtils
	 */
	public static JanusGraphUtils janus() {
		if (instance != null) {
			return instance;
		}

		synchronized (JanusGraphUtils.class) {
			if (instance != null) {
				return instance;
			}

			instance = new JanusGraphUtils();
			return instance;

		}
	}

	/**
	 * returning the JANUSGraphSession for default Janus cluster.
	 */
	public JANUSGraphSession template() {
		return context.get(DEFAULT_JANUS_GRAPH_PROPERTIES_BUNDLE_KEY);
	}

	public JANUSGraphSession template(String resourceName) {
		if (!context.containsKey(resourceName))
			throw new IllegalArgumentException("resource " + resourceName + " is not set");

		return context.get(resourceName);
	}

	/**
	 * Create a JANUSGraphSession
	 * 
	 * @param resourceName
	 * @param properties
	 * @return
	 * @throws TestConfigurationException
	 */
	private void registerJanusGraphSession(String resourceName, Properties properties) {
		if (context.containsKey(resourceName))
			throw new IllegalArgumentException(
					"resource " + resourceName + " already set in the context. Override is not allowed");

		String janusPassword = properties.getProperty(JANUS_GRAPH_PASSWORD);
		String remoteObjectsYamlPath = properties.getProperty(JANUS_REMOTE_OBJECTS_YAML_PATH);

		if (janusPassword == null) {
			throw new IllegalArgumentException("Property '" + JANUS_GRAPH_PASSWORD + "' undefined!");
		}

		final JANUSGraphClusterFactory factory = new JANUSGraphClusterFactory();
		JANUSGraphCluster cluster = null;

		// if the property janus.remote.objects.yaml.path is set, the JanusGraph
		// cluster is created from YAML configuration file located in the
		// specified path. Default the cluster
		// is created with basic information (simple way).
		if (StringUtils.isNotBlank(remoteObjectsYamlPath)) {

			YAMLConfiguration confYaml = null;
			try {
				confYaml = new YAMLConfiguration(remoteObjectsYamlPath);
			} catch (ConfigurationException e) {
				throw new TestConfigurationException("An exception occurred configuring test properties!", e);
			}
			cluster = factory.createCluster(confYaml);

		} else {
			cluster = factory.createCluster(properties.getProperty(JANUS_GRAPH_SERVER_HOST),
					Integer.valueOf(properties.getProperty(JANUS_GRAPH_SERVER_PORT)),
					properties.getProperty(JANUS_GRAPH_NAME), properties.getProperty(JANUS_GRAPH_USERNAME),
					janusPassword);
		}

		final JANUSGraphSession session = cluster.connect();

		context.put(resourceName, session);

	}

}
