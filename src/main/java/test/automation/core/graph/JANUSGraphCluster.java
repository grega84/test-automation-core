package test.automation.core.graph;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.io.FileNotFoundException;

import org.apache.commons.configuration.Configuration;
import org.apache.tinkerpop.gremlin.driver.Cluster;
import org.apache.tinkerpop.gremlin.driver.MessageSerializer;
import org.apache.tinkerpop.gremlin.structure.Graph;
import org.apache.tinkerpop.gremlin.driver.ser.GryoMessageSerializerV1d0;

public class JANUSGraphCluster implements GraphCluster<JANUSGraphSession> {

	private static final String JANUSGRAPH_IO_REGISTRY = "org.janusgraph.graphdb.tinkerpop.JanusGraphIoRegistry";
	private static final String KEY_REGISTRY = "ioRegistries";
	private static final String CONF_NULL = "conf is null!";
	protected Cluster cluster;

	/**
	 * open cluster from configuration
	 * 
	 * see <a
	 * href="http://tinkerpop.apache.org/docs/current/reference/#_configuration>
	 * configuration reference</a>
	 * 
	 * @param conf
	 *            configuration infos
	 * @throws FileNotFoundException
	 */
	protected JANUSGraphCluster(final Configuration conf) {
		Objects.requireNonNull(conf, CONF_NULL);

		cluster = Cluster.open(conf);
	}

	/**
	 * open cluster with basic information (simple way)
	 * 
	 * @param host
	 * @param port
	 * @param graph_name
	 * @param username
	 * @param password
	 * @return
	 */
	protected JANUSGraphCluster(String host, int port, String graph_name, String username, String password) {

		final MessageSerializer serializer = new GryoMessageSerializerV1d0();
		final Map<String, Object> config = new HashMap<>(1);
		final Map<String, Graph> graphs = Collections.emptyMap();

		config.put(KEY_REGISTRY, Arrays.asList(JANUSGRAPH_IO_REGISTRY));
		serializer.configure(config, graphs);

		cluster = Cluster.build(host).port(port).serializer(serializer).enableSsl(false).create();

	}

	@Override
	public void close() throws IOException {
		if (cluster != null)
			cluster.close();
	}

	@Override
	public JANUSGraphSession connect() {
		return JANUSGraphSession.of(cluster);
	}

}
