package test.automation.core.graph;

import java.io.FileNotFoundException;
import org.apache.commons.configuration.Configuration;

public class JANUSGraphClusterFactory {
	
    /**
     * open cluster with basic information (simple way)
     * 
     * @param host
     * @param port
     * @param graph_name
     * @param username
     * @param password
     * @return
     */
	public JANUSGraphCluster createCluster(String host, int port, String graph_name, String username, String password) {
		return new JANUSGraphCluster(host, port, graph_name, username, password);
	}
	
    /**
     * open cluster from configuration 
     * 
     * see <a href="http://tinkerpop.apache.org/docs/current/reference/#_configuration>configuration reference</a>
     * 
     * @param conf configuration infos
     * @throws FileNotFoundException 
     */
    public JANUSGraphCluster createCluster(final Configuration conf) {   
        return new JANUSGraphCluster(conf);
    }

}
