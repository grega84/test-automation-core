package test.automation.core.graph;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

import org.apache.commons.configuration.Configuration;
import org.apache.tinkerpop.gremlin.driver.Client;
import org.apache.tinkerpop.gremlin.driver.Cluster;
import org.apache.tinkerpop.gremlin.driver.ResultSet;
import org.apache.tinkerpop.gremlin.driver.remote.DriverRemoteConnection;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Graph;
import org.apache.tinkerpop.gremlin.structure.util.empty.EmptyGraph;

public class JANUSGraphSession
		implements GraphSession, GraphSessionClient<Client, ResultSet, CompletableFuture<ResultSet>> {

	private static final String CLUSTER_NULL = "cluster is null!";
	private static final String CLIENT_NULL = "'client' object is null!";
	private static final String TRAVERSAL_NULL = "'g' object is null!";
	private static final String GREMLIN_QUERY_NULL = "gremlinQuery is null!";
	private static final String DEFAULT_TRAVERSAL_SOURCE = "g";

	protected Graph graph;
	protected Client client;
	final GraphTraversalSource g;

	public static JANUSGraphSession of(Cluster cluster) {
		return new JANUSGraphSession(cluster);
	}

	private JANUSGraphSession(Cluster cluster, Configuration conf) {

		Objects.requireNonNull(cluster, CLUSTER_NULL);

		client = cluster.connect();
		// using the remote graph for queries
		graph = EmptyGraph.instance();
		g = graph.traversal().withRemote(conf);
	}

	private JANUSGraphSession(Cluster cluster) {

		Objects.requireNonNull(cluster, CLUSTER_NULL);
		// using DEFAULT_TRAVERSAL_SOURCE = "g"
		final DriverRemoteConnection remoteConnection = DriverRemoteConnection.using(cluster, DEFAULT_TRAVERSAL_SOURCE);

		client = cluster.connect();
		// using the remote graph for queries
		graph = EmptyGraph.instance();
		g = graph.traversal().withRemote(remoteConnection);
	}

	@Override
	public void close() throws IOException {

		if (client != null)
			client.close();
		if (g != null) {
			try {
				g.close();
			} catch (Exception e) {
				throw new IOException(e);
			}
		}
	}

	@Override
	public final Client getUnderlyingObject() {
		if (Objects.isNull(client))
			throw new IllegalStateException(CLIENT_NULL);

		return client;
	}

	@Override
	public final GraphTraversalSource G() {
		if (Objects.isNull(g))
			throw new IllegalStateException(TRAVERSAL_NULL);

		return g;
	}

	@Override
	public ResultSet executeGraph(String gremlinQuery) {
		Objects.requireNonNull(gremlinQuery, GREMLIN_QUERY_NULL);

		return getUnderlyingObject().submit(gremlinQuery);
	}

	@Override
	public ResultSet executeGraph(String gremlinQuery, Map<String, Object> parameters) {
		Objects.requireNonNull(gremlinQuery, GREMLIN_QUERY_NULL);

		return getUnderlyingObject().submit(gremlinQuery, parameters);

	}

	@Override
	public CompletableFuture<ResultSet> executeGraphAsync(String gremlinQuery, Map<String, Object> parameters) {
		Objects.requireNonNull(gremlinQuery, GREMLIN_QUERY_NULL);

		return getUnderlyingObject().submitAsync(gremlinQuery, parameters);
	}

}
