package test.automation.core;

import org.springframework.jms.core.JmsTemplate;

/**
 * This class provides utilities method to get pre-configured instances for
 * messaging systems. Supported messaging systems are: 1. JMS (default) 2. Kafka
 */
public class MessageUtils {

	private static MessageUtils instance;

	private MessageUtils() {

	}

	/**
	 * singleton factory method
	 * 
	 * @return single instance of MessageUtils
	 * @deprecated As of test-automation-core 1.6,
     * replaced by <code>MessageUtils.jms()</code> 
     * or <code>MessageUtils.kafka()</code>.
     */
	@Deprecated
	public static MessageUtils msg() {
		if (instance != null) {
			
			return instance;
		}

		synchronized (MessageUtils.class) {
			if (instance != null) {

				return instance;
			}

			instance = new MessageUtils();
			return instance;

		}
	}

	/**
	 * 
	 * @return single instance of JmsMessageUtils
	 */
	public static JmsMessageUtils jms() {
       
		return JmsMessageUtils.getInstance();
	}

	/**
	 * 
	 * @return single instance of KafkaMessageUtils
	 */
	public static <K, V> KafkaMessageUtils<K, V> kafka() {
        
		return KafkaMessageUtils.getInstance();
	}
    
	/**
	 * @deprecated As of test-automation-core 1.6,
	 * replaced by <code>MessageUtils.jms().template()</code> 
	 */
	@Deprecated
	public JmsTemplate template() {
		return JmsMessageUtils.getInstance().template();
	}
    
	/**
	 * @deprecated As of test-automation-core 1.6,
	 * replaced by <code>MessageUtils.jms().template(String resourceName)</code> 
	 */
	@Deprecated
	public JmsTemplate template(String resourceName) {
		return JmsMessageUtils.getInstance().template(resourceName);
	}
}
