package test.automation.core.kafka;

import java.util.Map;
import java.util.Properties;

import org.apache.kafka.clients.producer.ProducerConfig;

import test.automation.core.properties.FileMultiPropertiesLoaderImpl;
import test.automation.core.properties.MultiPropertiesLoader;

/**
 * This class loads properties file for Kafka server configuration from classpath. It
 * is used also for reading schema.registry.url property needed for Avro
 * serialization.
 * 
 *
 */

public class KafkaPropertiesReader {

	private static Map<String, Properties> propertiesBundles = null;

	private KafkaPropertiesReader() {

	}

	/**
	 * singleton factory method
	 * 
	 * @return single instance of properties bundles for Kafka configuration
	 */
	public static Map<String, Properties> read() {

		if (propertiesBundles != null) {

			return propertiesBundles;
		}

		synchronized (KafkaPropertiesReader.class) {
			if (propertiesBundles == null) {
				MultiPropertiesLoader multiPropertiesLoader = new FileMultiPropertiesLoaderImpl(
						(file) -> file.getName().split("-")[0]);

				propertiesBundles = multiPropertiesLoader.loadAndCheck(
						(d, name) -> name.endsWith("-kafka.properties"),
						ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
						ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
						ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
						KafkaConstants.SERIALIZATION_TYPE);

				if (!propertiesBundles
						.containsKey(KafkaConstants.DEFAULT_API_PROPERTIES_BUNDLE_KEY))
					throw new IllegalArgumentException(
							"No default-kafka.properties found in the "
									+ ((FileMultiPropertiesLoaderImpl) multiPropertiesLoader)
											.getPath());
			}

			return propertiesBundles;

		}
	}
}
