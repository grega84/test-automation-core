package test.automation.core.kafka;

/**
 * This class exposes constants to use for Kafka support implementation.
 * 
 */
public final class KafkaConstants {

	public static final String DEFAULT_API_PROPERTIES_BUNDLE_KEY = "default";
	public static final String SERIALIZATION_TYPE = "serialization.type";
	public static final String AVRO_SERIALIZATION = "avro";
	public static final String SCHEMA_REGISTRY_URL = "schema.registry.url";

}
