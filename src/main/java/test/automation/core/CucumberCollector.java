package test.automation.core;

import org.junit.rules.ErrorCollector;

/**
 * The CucumberCollector allows execution of a test to continue after the
 * first problem is found (for example, to collect _all_ the incorrect rows in a
 * table, and report them all at once):
 *
 * 
 *
 * &#064;Test
 * public void example() {
 *      collector.addError(new Throwable(&quot;first thing went wrong&quot;));
 *      collector.addError(new Throwable(&quot;second thing went wrong&quot;));
 *      collector.checkThat(getResult(), not(containsString(&quot;ERROR!&quot;)));
 *      // all lines will run, and then a combined failure logged at the end.
 *      
 *      collector.verifyAssertions();
 *     }
 * }
 * </pre>
 *
 * @since 1.1
 */
public class CucumberCollector extends ErrorCollector {

	public CucumberCollector() {
		super();
	}

	
	public void verifyAssertions() throws Throwable {
		super.verify();
	}

}
