package test.automation.core;

import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SafariUtils 
{
	private static final String RETURN="return";
	private static final String FIND_BY_XPATH="document.evaluate(\"${path}\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue";
	private static final String NE_NULL="!==null";
	private static final String SCROLL_INTO_VIEW="scrollIntoView({behavior: \"smooth\",block: \"end\",inline: \"end\"})";
	private static final String CLICK="click()";
	private static final String TEXT_EVENT="var textEvent = document.createEvent('TextEvent');textEvent.initTextEvent('textInput', true, true, null, \"${text}\");";
	private static final String DISPATCH_TEXT_EVENT="dispatchEvent(textEvent)";
	private static final String PAGE_LOAD="(document.readyState === \"complete\" || (document.readyState !== \"loading\" && !document.documentElement.doScroll)";
	
	public static final Function<WebDriver,Boolean> visibilityOfElement(WebDriver driver,String xpath)
	{
		return new Function<WebDriver, Boolean>() {

			@Override
			public Boolean apply(WebDriver arg0) 
			{
				return (Boolean) ((JavascriptExecutor)arg0).executeScript(RETURN+" "+FIND_BY_XPATH.replace("${path}", xpath)+NE_NULL);
			}
			
		};
	}
	
	public static final Function<WebDriver,Boolean> pageLoad(WebDriver driver)
	{
		return new Function<WebDriver,Boolean>(){

			@Override
			public Boolean apply(WebDriver arg0) 
			{
				return (Boolean) ((JavascriptExecutor)arg0).executeScript(RETURN+" "+PAGE_LOAD);
			}
			
		};
	}
	
	public <V> void waitForCondition(WebDriver driver, Function<? super WebDriver,V> expectedCondition,int seconds)
	{
		WebDriverWait wait=new WebDriverWait(driver, seconds);
		wait.until(expectedCondition);
		try {Thread.sleep(1000);} catch(Exception err) {}
	}
	
	public void click(WebDriver driver,String xpath)
	{
		((JavascriptExecutor)driver).executeScript(FIND_BY_XPATH.replace("${path}", xpath)+"."+CLICK);
		System.out.println("clicked on "+xpath);
	}
	
	public void scrollToElement(WebDriver driver,String xpath)
	{
		((JavascriptExecutor)driver).executeScript(FIND_BY_XPATH.replace("${path}", xpath)+"."+SCROLL_INTO_VIEW);
		System.out.println("scolled to "+xpath);
	}
	
	public WebElement scrollAndReturnElement(WebDriver driver,String xpath)
	{
		scrollToElement(driver, xpath);
		waitForCondition(driver, visibilityOfElement(driver, xpath), 10);
		return driver.findElement(By.xpath(xpath));
	}
	
	public void sendKeys(WebDriver driver,String xpath,String text)
	{
		((JavascriptExecutor)driver).executeScript(TEXT_EVENT.replace("${text}", text)+FIND_BY_XPATH.replace("${path}", xpath)+"."+DISPATCH_TEXT_EVENT+";");
		System.out.println("type text \""+text+"\" on element "+xpath);
	}
}
