package test.automation.core.video;

public enum VideoCodec 
{
	MP4(".mp4"),
	AVI(".avi");
	
	private String ext;
	
	private VideoCodec(String ext) 
	{
		this.ext=ext;
	}
	
	public String getExtension()
	{
		return ext;
	}
}
