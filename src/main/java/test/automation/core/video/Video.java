package test.automation.core.video;

public interface Video 
{
	void init();
	void start();
	void stop();
	VideoOption getSettings();
}
