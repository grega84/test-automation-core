package test.automation.core.video;

import java.io.File;

import test.automation.core.videorecorder.ExecFfmpeg;
import test.automation.core.videorecorder.VideoRecorder;

public class DesktopRecorder implements Video 
{
	private VideoOption opt;
	private VideoRecorder recorder;
	
	public DesktopRecorder(VideoOption opt) 
	{
		this.opt=opt;
		this.recorder=new VideoRecorder();
		this.recorder.setScreenId(opt.getScreenId());
		
	}

	@Override
	public void init() 
	{
		String fileNameAbsolutePath = opt.getVideoPath() + File.separatorChar + opt.getVideoName()+opt.getExtension().getExtension();
		this.recorder.init(fileNameAbsolutePath);
		System.out.println("start desktop recording: "+fileNameAbsolutePath);
	}

	@Override
	public void start() 
	{
		this.recorder.start();
	}

	@Override
	public void stop() 
	{
		this.recorder.stop();
		
		try {
			
			if(opt.isChangeFrameRate())
			{
				ExecFfmpeg.changeFrameRate(opt.getDeviceProperties(), opt.getVideoPath(), opt.getVideoName()+opt.getExtension().getExtension());
			}
			
			if(opt.isCrop())
			{
				ExecFfmpeg.cropVideo(opt.getDeviceProperties(), opt.getVideoPath(), opt.getVideoName()+opt.getExtension().getExtension());
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public VideoOption getSettings() 
	{
		return opt;
	}

}
