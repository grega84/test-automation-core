package test.automation.core.video;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

public class VideoOption 
{
	private VideoRecorderType type;
	private String videoName;
	private String videoPath;
	private boolean crop;
	private boolean changeFrameRate;
	private Properties deviceProperties;
	private WebDriver driver;
	private int screenId;
	private VideoCodec extension;
	
	public VideoOption(VideoRecorderType type, String videoName, String videoPath, boolean crop,
			boolean changeFrameRate, Properties deviceProperties, WebDriver driver, int screenId) {
		super();
		this.type = type;
		this.videoName = videoName;
		this.videoPath = videoPath;
		this.crop = crop;
		this.changeFrameRate = changeFrameRate;
		this.deviceProperties = deviceProperties;
		this.driver = driver;
		this.screenId = screenId;
		this.extension=VideoCodec.MP4;
	}
	

	public VideoOption(VideoRecorderType type, String videoName, String videoPath, boolean crop,
			boolean changeFrameRate, Properties deviceProperties, WebDriver driver, int screenId,
			VideoCodec extension) {
		super();
		this.type = type;
		this.videoName = videoName;
		this.videoPath = videoPath;
		this.crop = crop;
		this.changeFrameRate = changeFrameRate;
		this.deviceProperties = deviceProperties;
		this.driver = driver;
		this.screenId = screenId;
		this.extension = extension;
	}


	public VideoCodec getExtension() {
		return extension;
	}


	public void setExtension(VideoCodec extension) {
		this.extension = extension;
	}


	public VideoRecorderType getType() {
		return type;
	}

	public void setType(VideoRecorderType type) {
		this.type = type;
	}

	public String getVideoName() {
		return videoName;
	}

	public void setVideoName(String videoName) {
		this.videoName = videoName;
	}

	public String getVideoPath() {
		return videoPath;
	}

	public void setVideoPath(String videoPath) {
		this.videoPath = videoPath;
	}

	public boolean isCrop() {
		return crop;
	}

	public void setCrop(boolean crop) {
		this.crop = crop;
	}

	public boolean isChangeFrameRate() {
		return changeFrameRate;
	}

	public void setChangeFrameRate(boolean changeFrameRate) {
		this.changeFrameRate = changeFrameRate;
	}

	public Properties getDeviceProperties() {
		return deviceProperties;
	}

	public void setDeviceProperties(Properties deviceProperties) {
		this.deviceProperties = deviceProperties;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public int getScreenId() {
		return screenId;
	}

	public void setScreenId(int screenId) {
		this.screenId = screenId;
	}
	
	
}
