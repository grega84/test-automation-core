package test.automation.core.video;

import java.io.File;

import io.appium.java_client.AppiumDriver;
import test.automation.core.videorecorder.ExecFfmpeg;
import test.automation.core.videorecorder.mobile.MobileVideoRecorder;

public class MobileRecorder implements Video 
{
	private VideoOption opt;
	private MobileVideoRecorder recorder;
	

	public MobileRecorder(VideoOption opt) {
		super();
		this.opt = opt;
		this.recorder=new MobileVideoRecorder();
	}

	@Override
	public void init() 
	{
		String fileNameAbsolutePath = opt.getVideoPath() + File.separatorChar + opt.getVideoName()+opt.getExtension().getExtension();
		this.recorder.init(fileNameAbsolutePath);
		System.out.println("start mobile recording: "+fileNameAbsolutePath);
	}

	@Override
	public void start() 
	{
		this.recorder.start((AppiumDriver<?>) opt.getDriver());
	}

	@Override
	public void stop() 
	{
		this.recorder.stop();
		
		try {
			
			if(opt.isChangeFrameRate())
			{
				ExecFfmpeg.changeFrameRate(opt.getDeviceProperties(), opt.getVideoPath(), opt.getVideoName()+opt.getExtension().getExtension());
			}
			
			if(opt.isCrop())
			{
				ExecFfmpeg.cropVideo(opt.getDeviceProperties(), opt.getVideoPath(), opt.getVideoName()+opt.getExtension().getExtension());
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public VideoOption getSettings() 
	{
		return opt;
	}

}
