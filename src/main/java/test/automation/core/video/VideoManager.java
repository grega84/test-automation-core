package test.automation.core.video;

import java.io.File;
import java.util.HashMap;

public class VideoManager 
{
	private static VideoManager singleton;
	private HashMap<String, Video> videoRecorders;
	
	private VideoManager()
	{
		videoRecorders=new HashMap<>();
	}
	
	public static VideoManager get()
	{
		if(singleton == null)
			singleton=new VideoManager();
		
		return singleton;
	}
	
	public Video createVideoRecorder(VideoOption options)
	{
		Video vid=null;
		
		switch(options.getType())
		{
		case DESKTOP:
			vid=new DesktopRecorder(options);
			break;
		case MOBILE:
			vid=new MobileRecorder(options);
			break;
		}
		
		String videoPath=options.getVideoPath()+File.separatorChar+options.getVideoName()+options.getExtension().getExtension();
		videoRecorders.put(videoPath, vid);
		
		return vid;
	}
	
	public void clearAll()
	{
		videoRecorders.clear();
	}
	
	public Video getVideoRecorder(String videoPath)
	{
		return videoRecorders.get(videoPath);
	}
	
	public Video getVideoRecorder(VideoOption options)
	{
		String videoPath=options.getVideoPath()+File.separatorChar+options.getVideoName()+options.getExtension().getExtension();
		return videoRecorders.get(videoPath);
	}
}
