package test.automation.core;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.jms.ConnectionFactory;
import javax.naming.Context;

import org.springframework.jms.connection.SingleConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jndi.JndiObjectFactoryBean;

import test.automation.core.properties.FileMultiPropertiesLoaderImpl;
import test.automation.core.properties.MultiPropertiesLoader;

/**
 * Wrap spring jdbc jms utils class such as JMSTemplate. Vendor specific connection factory classes are required 
 * to build JMS ConnectionFactory instances. The current implementation does not support ConnectionFactory retrieval 
 * through JNDI. 
 * Supported ConnectiontFactory creation are:
 *  1. ActiveMQ = org.apache.activemq.ActiveMQConnectionFactory
 *  2. WebSphereMQ
 *  3. SAG Universal Messaging = com.pcbsys.nirvana.nSpace.NirvanaContextFactory
 */
public class JmsMessageUtils {
  
  private static JmsMessageUtils instance;
	
  private Map<String, JmsTemplate> context;
  
  private final String DEFAULT_API_PROPERTIES_BUNDLE_KEY = "default";
  
  //3 seconds as receive wait timeout 
  private final long DEFAULT_MESSAGE_RECEIVE_TIMEOUT = 3000;
  
  private static final String JMS_CONNECTIONFACTORY_JNDI_PROPERTY = "jms.connectionfactory.jndiname";
    
  private JmsMessageUtils() {
    context = new HashMap<String, JmsTemplate>();
    MultiPropertiesLoader multiPropertiesLoader = new FileMultiPropertiesLoaderImpl(
        (file) -> file.getName().split("-")[0]);
    
    Map<String, Properties> propertiesBundles = multiPropertiesLoader.loadAndCheck(
        (d, name) -> name.endsWith("-jms.properties"),
        Context.INITIAL_CONTEXT_FACTORY,
        Context.PROVIDER_URL,
        JMS_CONNECTIONFACTORY_JNDI_PROPERTY);
    
    if (!propertiesBundles.containsKey(DEFAULT_API_PROPERTIES_BUNDLE_KEY))
      throw new IllegalArgumentException("No default-jms.properties found in the "
          + ((FileMultiPropertiesLoaderImpl) multiPropertiesLoader).getPath());
    
    for (Entry<String, Properties> propertyBundle : propertiesBundles.entrySet()) {
      registerJMSTemplate(propertyBundle.getKey(), propertyBundle.getValue());
      
    }
    
  }
  
	/**
	 * singleton factory method
	 * 
	 * @return single instance of JmsMessageUtils
	 */
	public static JmsMessageUtils getInstance() {
		if (instance != null) {
			return instance;
		}
	
		synchronized (JmsMessageUtils.class) {
			if (instance != null) {
				return instance;
			}
	
			instance = new JmsMessageUtils();
			return instance;
	
		}
	}
  
  public JmsTemplate template() {
    return context.get(DEFAULT_API_PROPERTIES_BUNDLE_KEY);
  }
  
  public JmsTemplate template(String resourceName) {
    if (!context.containsKey(resourceName))
      throw new IllegalArgumentException("resource " + resourceName + " is not set");
    
    return context.get(resourceName);
  }
  
  private void registerJMSTemplate(String resourceName, Properties properties) {
    if (context.containsKey(resourceName))
      throw new IllegalArgumentException(
          "resource " + resourceName + " already set in the context. Override is not allowed");
    
    //Create specific properties for java standard JNDI context lookup
    Properties jndiProperties = new Properties();
    jndiProperties.setProperty(Context.INITIAL_CONTEXT_FACTORY,
        properties.getProperty(Context.INITIAL_CONTEXT_FACTORY));
    jndiProperties.setProperty(Context.PROVIDER_URL, properties.getProperty(Context.PROVIDER_URL));
    
    // Use spring JNDI facilities for connection factory lookup request
    JndiObjectFactoryBean jndiObjectFactoryBean = new JndiObjectFactoryBean();
    jndiObjectFactoryBean.setJndiEnvironment(properties);
    jndiObjectFactoryBean.setExpectedType(ConnectionFactory.class);
    jndiObjectFactoryBean.setLookupOnStartup(false);
    jndiObjectFactoryBean.setJndiName(properties.getProperty(JMS_CONNECTIONFACTORY_JNDI_PROPERTY));
    jndiObjectFactoryBean.setResourceRef(false);
    jndiObjectFactoryBean.setCache(true);
    
    //Starting jndi lookup
    try {
      jndiObjectFactoryBean.afterPropertiesSet();
    } catch (Exception e) {
      String errorMessage = "Error during jms connection factory [" + jndiObjectFactoryBean.getJndiName() +
          "] retrieval from jndi server [" + properties.getProperty(Context.PROVIDER_URL) + "]";
      throw new RuntimeException(errorMessage, e);
    }
    
    ConnectionFactory connectionFactory = (ConnectionFactory) jndiObjectFactoryBean.getObject();
    
    //Create JMS Template instance
    SingleConnectionFactory singleConnectionFactory = new SingleConnectionFactory(connectionFactory);
    JmsTemplate jmsTemplate = new JmsTemplate(singleConnectionFactory);
    jmsTemplate.setReceiveTimeout(DEFAULT_MESSAGE_RECEIVE_TIMEOUT);
    
    context.put(resourceName, jmsTemplate);
    
  }
  
}
