package test.automation.core;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import test.automation.core.properties.FileMultiPropertiesLoaderImpl;
import test.automation.core.properties.MultiPropertiesLoader;
import test.automation.core.ssh.SSHIllegalArgumentException;
import test.automation.core.ssh.SSHSession;
import test.automation.core.ssh.SSHSessionImpl;

public class SSHUtils {
	private static SSHUtils instance;
	private Map<String, SSHSession> context;
	private final static String SSH_HOST = "ssh.host";
	private final static String DEFAULT_SSH_PROPERTIES_BUNDLE_KEY = "default";
	private Map<String, Properties> propertiesBundles = null;

	/**
	 * Loading properties file from classpath.
	 */
	private SSHUtils() {
		// load properties
		context = new HashMap<String, SSHSession>();
		MultiPropertiesLoader multiPropertiesLoader = new FileMultiPropertiesLoaderImpl(
				(file) -> file.getName().split("-")[0]);
		propertiesBundles = multiPropertiesLoader.loadAndCheck((d, name) -> name.endsWith("-ssh.properties"), SSH_HOST);
		if (!propertiesBundles.containsKey(DEFAULT_SSH_PROPERTIES_BUNDLE_KEY)) {
			throw new SSHIllegalArgumentException("No default-ssh.properties found in the "
					.concat(((FileMultiPropertiesLoaderImpl) multiPropertiesLoader).getPath()));
		}
		propertiesBundles.forEach((k, v) -> context.put(k, initTemplate(v)));
	}

	/**
	 * Initialize session reading default-ssh.properties file
	 * 
	 * @return this
	 */
	public SSHSession template() {
		return context.get(DEFAULT_SSH_PROPERTIES_BUNDLE_KEY);
	}

	/**
	 * Initialize session reading <templateKey>-ssh.properties file
	 * 
	 * @param templateKey
	 * @return this
	 */
	public SSHSession template(String templateKey) {
		if (!context.containsKey(templateKey))
			throw new SSHIllegalArgumentException("Unable to find properties file " + templateKey + "-ssh.propeties");
		return context.get(templateKey);
	}

	/**
	 * template initialization with properties
	 * 
	 * @param properties
	 * @return SSHSessionImpl
	 */
	private SSHSession initTemplate(Properties properties) {
		return new SSHSessionImpl(properties);
	}

	/**
	 * Singleton factory method
	 * 
	 * @return Single instance of SSHUtils
	 */
	public static SSHUtils ssh() {
		if (instance != null) {
			return instance;
		}
		synchronized (SSHUtils.class) {
			if (instance != null) {
				return instance;
			}
			instance = new SSHUtils();
			return instance;
		}
	}
}