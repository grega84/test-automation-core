/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.automation.core.videorecorder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.text.StrSubstitutor;
import org.springframework.util.Assert;

import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import net.bramp.ffmpeg.probe.FFmpegProbeResult;

/**
 *
 * @author enyason
 */
public class ExecFfmpeg {


	private static final String SEEING_CMD = "ffmpeg -y -i \"${video}\" -c copy -f h264 \"${seeing}\"";//seeing_noaudio.h264
	private static final String CHANGE_FRAME_RATE = "ffmpeg -y -r 6 -i \"${seeing}\" -c copy \"${seeing_mp4}\"";
	private static final String DELETE_TMP_FILE = "cmd /c del /f \"${seeing}\"";
	private static final String DELETE_TMP_FILE_UNIX = "/bin/rm ${seeing}";

	private static final String DELETE_OLD_FILE = "cmd /c del /f \"${video}\"";
	private static final String RENAME_NEW_FILE = "cmd /c ren \"${seeing_mp4}\" \"${video}\"";
	private static final String CROP_VIDEO = "ffmpeg -i \"${video}\" -filter:v \"crop=${width}:${height}:${x}:${y}\" \"${cropped}\"";

	private static final String SEEING_FILE="seeing_noaudio.h264";
	private static final String SEEING_MP4_FILE="seeing.mp4";
	private static final String CROPPING_FILE="crop.mp4";
	private static final String FFMPEG_PATH = "video.ffmpeg.path";
	private static final String FFPROBE_PATH = "video.ffprobe.path";
	private static final String VIDEO_SIZE = "video.size";
	private static final String CROP_ARGS = "\"crop=${width}:${height}:${x}:${y}\"";
	private static final String DELETE_OLD_FILE_UNIX = "/bin/rm ${video}";
	private static final String RENAME_NEW_FILE_UNIX = "/bin/mv ${seeing_mp4} ${video}";
	private static final String CROP_ARGS_UNIX = "crop=${width}:${height}:${x}:${y}";

	public static void executeFfMpeg(String exec) {

		Runtime runtime = Runtime.getRuntime();
		System.out.println("run command "+exec);

		try {
			Process process = runtime.exec(exec);

			int exitVal = process.waitFor();
			System.out.println("Exited with code " + exitVal);

		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}

	}

	public static void changeFrameRate(Properties deviceProp,String videoFolder,String videoName) throws Exception
	{
		Assert.isTrue(deviceProp != null);
		Assert.isTrue(deviceProp.getProperty(FFMPEG_PATH) != null);
		Assert.isTrue(deviceProp.getProperty(FFPROBE_PATH) != null);

		String fileName="V_"+Calendar.getInstance().getTimeInMillis();

		FFmpeg ffmpeg = new FFmpeg(deviceProp.getProperty(FFMPEG_PATH));
		FFprobe ffprobe = new FFprobe(deviceProp.getProperty(FFPROBE_PATH));

		FFmpegExecutor executor=new FFmpegExecutor(ffmpeg);

		FFmpegBuilder builder = new FFmpegBuilder();
		FFmpegProbeResult in = ffprobe.probe(videoFolder+File.separatorChar+videoName);

		System.out.println("start seeing video");
		//SEEING_CMD
		builder.addInput(in)
		.addExtraArgs("-y")
		//		.addOutput(videoFolder+File.separatorChar+SEEING_FILE)
		.addOutput(videoFolder+File.separatorChar+fileName+".h264")
		.addExtraArgs("-c","copy")
		.addExtraArgs("-f","h264")
		.done();

		// Run a one-pass encode
		executor.createJob(builder).run();

		System.out.println("end seeing video");

		//		in=ffprobe.probe(videoFolder+File.separatorChar+SEEING_FILE);

		in=ffprobe.probe(videoFolder+File.separatorChar+fileName+".h264");

		System.out.println("start change frame rate video");

		builder=new FFmpegBuilder();

		//CHANGE_FRAME_RATE
		builder.addInput(in)
		.addExtraArgs("-y")
		.addExtraArgs("-r","6")
		//		.addOutput(videoFolder+File.separatorChar+SEEING_MP4_FILE)
		.addOutput(videoFolder+File.separatorChar+fileName+".mp4")
		.addExtraArgs("-c","copy")
		.done();

		// Run a one-pass encode
		executor.createJob(builder).run();

		System.out.println("end change frame rate video");

		//remove files
		Map<String, String> map= new HashMap<String, String>();
		//		map.put("seeing", videoFolder+File.separatorChar+SEEING_FILE);
		map.put("seeing", videoFolder+File.separatorChar+fileName+".h264");

		if(isWindows())
			executeFfMpeg(stringFilter(DELETE_TMP_FILE, map));
		else
			executeFfMpeg(stringFilter(DELETE_TMP_FILE_UNIX, map));

		map.clear();
		map.put("video", videoFolder+File.separatorChar+videoName);

		if(isWindows())
			executeFfMpeg(stringFilter(DELETE_OLD_FILE, map));
		else
			executeFfMpeg(stringFilter(DELETE_OLD_FILE_UNIX, map));

		//rename new file
		map.clear();
		//		map.put("seeing_mp4", videoFolder+File.separatorChar+SEEING_MP4_FILE);
		map.put("seeing_mp4", videoFolder+File.separatorChar+fileName+".mp4");
		map.put("video", videoName);

		if(isWindows())
			executeFfMpeg(stringFilter(RENAME_NEW_FILE, map));
		else
		{
			map.put("video",videoFolder+File.separatorChar+videoName);
			executeFfMpeg(stringFilter(RENAME_NEW_FILE_UNIX, map));
		}
	}

	private static boolean isWindows() 
	{
		// TODO Auto-generated method stub
		return System.getProperty("os.name").startsWith("Windows");
	}

	public static void cropVideo(Properties deviceProp,String videoFolder,String videoName) throws Exception
	{
		Assert.isTrue(deviceProp != null);
		Assert.isTrue(deviceProp.getProperty(FFMPEG_PATH) != null);
		Assert.isTrue(deviceProp.getProperty(FFPROBE_PATH) != null);
		Assert.isTrue(deviceProp.getProperty(VIDEO_SIZE) != null);

		String fileName="V_"+Calendar.getInstance().getTimeInMillis();

		FFmpeg ffmpeg = new FFmpeg(deviceProp.getProperty(FFMPEG_PATH));
		FFprobe ffprobe = new FFprobe(deviceProp.getProperty(FFPROBE_PATH));
		String[] size=deviceProp.getProperty(VIDEO_SIZE).split(",");
		Assert.isTrue(size.length == 4);

		FFmpegExecutor executor=new FFmpegExecutor(ffmpeg);

		FFmpegBuilder builder = new FFmpegBuilder();
		FFmpegProbeResult in = ffprobe.probe(videoFolder+File.separatorChar+videoName);

		System.out.println("start crop video");

		Map<String, String> map= new HashMap<String, String>();
		map.put("x", size[0]);
		map.put("y", size[1]);
		map.put("width", size[2]);
		map.put("height", size[3]);

		if(isWindows())
		{
			builder.addInput(in)
			//		.addOutput(videoFolder+File.separatorChar+CROPPING_FILE)
			.addOutput(videoFolder+File.separatorChar+fileName+".mp4")
			.addExtraArgs("-filter:v",stringFilter(CROP_ARGS, map))
			.done();
		}
		else
		{
			builder.addInput(in)
			//		.addOutput(videoFolder+File.separatorChar+CROPPING_FILE)
			.addOutput(videoFolder+File.separatorChar+fileName+".mp4")
			.addExtraArgs("-filter:v",stringFilter(CROP_ARGS_UNIX, map))
			.done();
		}

		// Run a one-pass encode
		executor.createJob(builder).run();

		System.out.println("end crop video");

		map.clear();
		map.put("video", videoFolder+File.separatorChar+videoName);

		if(isWindows())
			executeFfMpeg(stringFilter(DELETE_OLD_FILE, map));
		else
			executeFfMpeg(stringFilter(DELETE_OLD_FILE_UNIX, map));

		//rename new file
		map.clear();
		//		map.put("seeing_mp4", videoFolder+File.separatorChar+CROPPING_FILE);
		map.put("seeing_mp4", videoFolder+File.separatorChar+fileName+".mp4");
		map.put("video", videoName);

		if(isWindows())
			executeFfMpeg(stringFilter(RENAME_NEW_FILE, map));
		else
		{
			map.put("video",videoFolder+File.separatorChar+videoName);
			executeFfMpeg(stringFilter(RENAME_NEW_FILE_UNIX, map));
		}

	}

	public static void concatVideos(Properties deviceProp,String videoFolder,String videoName,String...files) throws Exception
	{
		Assert.isTrue(deviceProp != null);
		Assert.isTrue(deviceProp.getProperty(FFMPEG_PATH) != null);
		//Assert.isTrue(deviceProp.getProperty(FFPROBE_PATH) != null);
		//Assert.isTrue(deviceProp.getProperty(VIDEO_SIZE) != null);

		FFmpeg ffmpeg = new FFmpeg(deviceProp.getProperty(FFMPEG_PATH));
		//FFprobe ffprobe = new FFprobe(deviceProp.getProperty(FFPROBE_PATH));

		String fileName="V_"+Calendar.getInstance().getTimeInMillis();

		String echoFile="";

		for(String file : files)
		{
			if(file != null)
				echoFile +="file '"+file+"'"+System.lineSeparator();
		}

		StringBuilder sb = new StringBuilder();
		sb.append(videoFolder).append(File.separatorChar).append("myList.txt");

		String txt=sb.toString();

		System.out.println(txt);

		File f=new File(txt);

		FileWriter writer=null;
		BufferedWriter bw=null;
		try
		{
			if(!f.exists())
				f.createNewFile();

			writer= new FileWriter(sb.toString());
			bw = new BufferedWriter(writer);
			bw.write(echoFile);

			bw.close();
			writer.close();

		} catch (IOException e) {
			System.err.format("IOException: %s%n", e);
		}
		finally
		{
			try
			{
				if(writer != null)
					writer.close();
				if(bw != null)
					bw.close();
			}
			catch(Exception err)
			{

			}
		}

		FFmpegExecutor executor=new FFmpegExecutor(ffmpeg);
		FFmpegBuilder builder = new FFmpegBuilder();

		sb = new StringBuilder();
		//	   	sb.append(videoFolder).append(File.separatorChar).append("out.mp4");
		sb.append(videoFolder).append(File.separatorChar).append(fileName+".mp4");

		String out=sb.toString();

		System.out.println("start concat video");
		//CONCAT_CMD
		builder.addInput("\""+txt+"\"")
		.addExtraArgs("-f","concat")
		.addExtraArgs("-safe","0")
		.addOutput("\""+out+"\"")
		.addExtraArgs("-c","copy")
		.done();

		// Run a one-pass encode
		executor.createJob(builder).run();

		sb = new StringBuilder();
		sb.append(videoFolder).append(File.separatorChar).append(videoName);

		ExecFfmpeg.executeFfMpeg("cmd /c del /f \""+sb.toString()+"\"");
		ExecFfmpeg.executeFfMpeg("cmd /c ren \""+out+"\" \""+videoName+"\"");


	}

	public static final String stringFilter(String template, Map<String, String> map) 
	{
		StrSubstitutor sub = new StrSubstitutor(map);
		return sub.replace(template);
	}

}
