
package test.automation.core.videorecorder.mobile;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Base64;
import java.util.TimerTask;
import java.util.Base64.Decoder;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import org.jcodec.api.awt.AWTSequenceEncoder;
import org.openqa.selenium.OutputType;

import io.appium.java_client.AppiumDriver;
import test.automation.core.videorecorder.RecordTimer;

/**
 *
 * @author enyason
 */
public class MobileScreenRecorderTask extends TimerTask {

    AWTSequenceEncoder encoder;

    //Robot robot;
    Rectangle screenDimension;
    BufferedImage image;

	private String adbCommand;
    
    public MobileScreenRecorderTask(AWTSequenceEncoder sequenceEncoder,String adbCommand) 
    {
        encoder = sequenceEncoder;
        this.adbCommand=adbCommand;
        RecordTimer.start();
    }

    @Override
    public void run() {
               
        System.out.println("encoding mobile...");
        
        
        
        String data=getRawImageFromDevice();
 	   
 	   if(data == null) return;
 	   
//       if(MainScreenRecorderFrame.timerRecord != null){
       
       try {
    	   
    	   Decoder decoder = Base64.getDecoder();
           byte[] imageByte = decoder.decode(data);
           ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
           
    	    image=ImageIO.read(bis);
            encoder.encodeImage(image);
        } catch (Exception ex) {
            Logger.getLogger(MobileScreenRecorderTask.class.getName()).log(Level.SEVERE, null, ex);
        }
       
//       }


    }

	private String getRawImageFromDevice() 
	{
		try {
			
        	Process p = Runtime.getRuntime().exec(adbCommand);

        	BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
	   	     String line = null; 
	   	     String b="";
	   	     
	   	     try {
	   	        while ((line = input.readLine()) != null)
	   	        {
	   	        	b+=line;
	   	        }
	   	        
	   	     } catch (IOException e) {
	   	            e.printStackTrace();
	   	            return null;
	   	     }
	   	    
	   	     return b;
	   	     
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

}
