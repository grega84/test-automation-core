package test.automation.core.videorecorder.mobile;

import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.Base64;
import java.util.Timer;
import java.util.Base64.Decoder;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JLabel;

import org.apache.commons.io.FileUtils;
import org.jcodec.api.awt.AWTSequenceEncoder;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidStartScreenRecordingOptions;
import io.appium.java_client.ios.IOSStartScreenRecordingOptions;
import io.appium.java_client.screenrecording.BaseStartScreenRecordingOptions;
import io.appium.java_client.screenrecording.CanRecordScreen;
import test.automation.core.videorecorder.RecordTimer;
import test.automation.core.videorecorder.TimerCountTask;


public class MobileVideoRecorder 
{
	private boolean isRecording;
	private File f;
	private AppiumDriver<?> driver;
	
	public void init(String fileName)
	{
		//rectangle = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
		this.driver=driver;

//      //create a new file
       f = new File(fileName);
	}

	public void start(AppiumDriver<?> driver)
	{
		this.driver=driver;
		
		if(driver instanceof AndroidDriver<?>)
		((CanRecordScreen) driver).startRecordingScreen(new AndroidStartScreenRecordingOptions()
				.withTimeLimit(Duration.ofMinutes(20))
				.withVideoSize(driver.manage().window().getSize().width+"x"+driver.manage().window().getSize().height));
		else
			((CanRecordScreen) driver).startRecordingScreen(new IOSStartScreenRecordingOptions()
					.withTimeLimit(Duration.ofMinutes(20))
					.withVideoType("libx264")
					.withVideoScale("1280:720"));
			
		isRecording = true;

		/*
		isRecording = true;

        int delay = 1000 / 60;

        RecordTimer.reset();

        timerRecord = new Timer("Thread TimerRecord Mobile");

        timerCount = new Timer("Thread TimerCount Mobile");

        recorderTask = new MobileScreenRecorderTask(encoder, adbCommand);
        //countTask = new TimerCountTask(labelTimer);

        timerRecord.scheduleAtFixedRate(recorderTask, 0, delay);
        //timerCount.scheduleAtFixedRate(countTask, 0, 1000);
*/
	}
	
	public void stop()
	{
		String s=((CanRecordScreen) driver).stopRecordingScreen();
		
		Decoder decoder = Base64.getDecoder();
        byte[] b = decoder.decode(s);
        
        try {
			FileUtils.writeByteArrayToFile(f, b);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*
		RecordTimer.stop();

        //cancle the timer for time counting here
        //timerCount.cancel();
        //timerCount.purge();

        // stop the timer from executing the task here
        timerRecord.cancel();
        timerRecord.purge();

        recorderTask.cancel();
        //countTask.cancel();

        try {

            encoder.finish();// finish  encoding
            System.out.println("encoding finish " + (RecordTimer.getTimeInSec()) + "s");

        } catch (IOException ex) {
           
        }
*/
	}
}
