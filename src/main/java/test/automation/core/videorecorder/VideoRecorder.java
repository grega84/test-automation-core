package test.automation.core.videorecorder;

import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JLabel;

import org.apache.commons.lang3.StringUtils;
import org.jcodec.api.awt.AWTSequenceEncoder;
import org.springframework.util.Assert;

public class VideoRecorder 
{
	private static final String VIDEO_SIZE = "video.size";//x,y,w,h
	private boolean isRecording;
	private Timer timerRecord;
	private Timer timerCount;
	private ScreenRecorderTask recorderTask;
	private TimerCountTask countTask;
	private Rectangle rectangle;
	private JLabel labelTimer;
	private AWTSequenceEncoder encoder;
	private File f;
	
	private int screenId=-1;
	
	public void init(String fileName)
	{
		rectangle = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
		
		if(screenId != -1)
		{
			//Take a screenshot of every monitor
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			GraphicsDevice[] screens = ge.getScreenDevices();
			
			Assert.isTrue(screenId < screens.length && screenId >= 0, "incorrect screenId:"+screenId);
			
			rectangle = screens[screenId].getDefaultConfiguration().getBounds();
		}

//      //create a new file
       f = new File(fileName);

       try {
           // initialize the encoder
           encoder = AWTSequenceEncoder.createSequenceEncoder(f, 24 / 8);
       } catch (IOException ex) {
          ex.printStackTrace();
       }
	}
	
	

	public int getScreenId() {
		return screenId;
	}



	public void setScreenId(int screenId) {
		this.screenId = screenId;
	}



	public void start()
	{
		isRecording = true;

        int delay = 1000 / 24;

        RecordTimer.reset();

        timerRecord = new Timer("Thread TimerRecord");

        timerCount = new Timer("Thread TimerCount");

        recorderTask = new ScreenRecorderTask(encoder, rectangle);
        countTask = new TimerCountTask(labelTimer);

        timerRecord.scheduleAtFixedRate(recorderTask, 0, delay);
        timerCount.scheduleAtFixedRate(countTask, 0, 1000);

	}
	
	public void stop()
	{
		RecordTimer.stop();

        //cancle the timer for time counting here
        timerCount.cancel();
        timerCount.purge();

        // stop the timer from executing the task here
        timerRecord.cancel();
        timerRecord.purge();

        recorderTask.cancel();
        countTask.cancel();

        try {

            encoder.finish();// finish  encoding
            System.out.println("encoding finish " + (RecordTimer.getTimeInSec()) + "s");

        } catch (IOException ex) {
           
        }

	}
}
