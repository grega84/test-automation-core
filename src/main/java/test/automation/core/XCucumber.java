package test.automation.core;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.List;

import org.junit.runners.model.InitializationError;

import cucumber.api.junit.Cucumber;
import cucumber.runtime.Runtime;
import cucumber.runtime.RuntimeOptions;
import cucumber.runtime.io.ResourceLoader;
import test.automation.core.annotation.Api;
import test.automation.core.annotation.Ui;

public class XCucumber extends Cucumber{
	
	public XCucumber(Class clazz) throws InitializationError, IOException {
		super(clazz);
	}

	@Override
	protected Runtime createRuntime(ResourceLoader resourceLoader, ClassLoader classLoader,
			RuntimeOptions runtimeOptions) throws InitializationError, IOException {
		List<String> glue = runtimeOptions.getGlue();
		//adding common hook
		glue.add(0,"classpath:com.test.automation.core.hook.common");
		
		Annotation[] runnerAnnotations = this.getRunnerAnnotations();
		boolean uiAnnotationPresent = Arrays.stream(runnerAnnotations).anyMatch(a -> a.annotationType().getName().equals(Ui.class.getName()));
		boolean apiAnnotationPresent = Arrays.stream(runnerAnnotations).anyMatch(a -> a.annotationType().getName().equals(Api.class.getName()));
		
		if (uiAnnotationPresent){
			//adding ui hook
			glue.add(0,"classpath:com.test.automation.core.hook.ui");
		}else if(apiAnnotationPresent){
			//adding api hook
			glue.add(0,"classpath:com.test.automation.core.hook.api");
		}
		return super.createRuntime(resourceLoader, classLoader, runtimeOptions);
	}

}
