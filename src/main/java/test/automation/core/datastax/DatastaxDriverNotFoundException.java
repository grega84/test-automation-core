package test.automation.core.datastax;

/**
 * This class provides a custom exception to notify the absence of datastax
 * driver class (Community or Enterprise) in the classpath
 * 
 *
 */

@SuppressWarnings("serial")
public class DatastaxDriverNotFoundException extends Exception {
	
	public DatastaxDriverNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public DatastaxDriverNotFoundException(Throwable cause) {
		super(cause);
	}
	
	public DatastaxDriverNotFoundException(String message) {
		super(message);
	}
	
	public DatastaxDriverNotFoundException() {
		super();
	}

}
