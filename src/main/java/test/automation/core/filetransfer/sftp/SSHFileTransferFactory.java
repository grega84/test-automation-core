package test.automation.core.filetransfer.sftp;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import test.automation.core.properties.FileMultiPropertiesLoaderImpl;
import test.automation.core.properties.MultiPropertiesLoader;
import test.automation.core.ssh.SSHIllegalArgumentException;

public class SSHFileTransferFactory {
	private static SSHFileTransferFactory instance;
	private Map<String, SSHFileTransfer> context;
	private final static String SSH_HOST = "ssh.host";
	private final static String DEFAULT_SFTP_PROPERTIES_BUNDLE_KEY = "default";
	private Map<String, Properties> propertiesBundles = null;

	/**
	 * singleton factory method
	 * 
	 * @return single instance of SSHFileTransferUtils
	 */
	public static SSHFileTransferFactory getInstance() {
		if (instance != null) {
			return instance;
		}
		synchronized (SSHFileTransferFactory.class) {
			if (instance != null) {
				return instance;
			}
			instance = new SSHFileTransferFactory();
			return instance;
		}
	}

	private SSHFileTransferFactory() {
		// load properties
		context = new HashMap<String, SSHFileTransfer>();
		MultiPropertiesLoader multiPropertiesLoader = new FileMultiPropertiesLoaderImpl(
				(file) -> file.getName().split("-")[0]);
		propertiesBundles = multiPropertiesLoader.loadAndCheck((d, name) -> name.endsWith("-sftp.properties"),
				SSH_HOST);
		if (!propertiesBundles.containsKey(DEFAULT_SFTP_PROPERTIES_BUNDLE_KEY)) {
			throw new SSHIllegalArgumentException("No default-sftp.properties found in the "
					.concat(((FileMultiPropertiesLoaderImpl) multiPropertiesLoader).getPath()));
		}
		propertiesBundles.forEach((k, v) -> context.put(k, initTemplate(v)));
	}

	private SSHFileTransfer initTemplate(Properties properties) {
		return new SSHFileTransfer(properties);
	}

	public SSHFileTransfer template() {
		return context.get(DEFAULT_SFTP_PROPERTIES_BUNDLE_KEY);
	}

	public SSHFileTransfer template(String resourceName) {
		if (!context.containsKey(resourceName))
			throw new SSHIllegalArgumentException("Unable to find properties file " + resourceName + "-sftp.properties");
		return context.get(resourceName);
	}
}
