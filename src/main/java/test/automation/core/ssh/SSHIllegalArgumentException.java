package test.automation.core.ssh;

public class SSHIllegalArgumentException extends IllegalArgumentException {
	private static final long serialVersionUID = 1L;

	public SSHIllegalArgumentException() {
		super();
	}

	public SSHIllegalArgumentException(String message) {
		super(message);
	}

	public SSHIllegalArgumentException(String message, Throwable cause) {
		super(message, cause);
	}

	public SSHIllegalArgumentException(Throwable cause) {
		super(cause);
	}
}
