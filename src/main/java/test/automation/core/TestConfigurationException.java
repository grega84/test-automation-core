package test.automation.core;

/**
 * This class provides a custom exception to notify the incorrect properties
 * configuration for test execution.
 * 
 *
 */

@SuppressWarnings("serial")
public class TestConfigurationException extends RuntimeException {

	public TestConfigurationException(String message, Throwable cause) {
		super(message, cause);
	}

	public TestConfigurationException(Throwable cause) {
		super(cause);
	}

	public TestConfigurationException(String message) {
		super(message);
	}

	public TestConfigurationException() {
		super();
	}

}
