package test.automation.core.cmd;

public class ProcessThread extends Thread 
{
	private Process p;
	private int state;
	private boolean end;
	
	public ProcessThread(Process p) 
	{
		this.p=p;
		state=-1;
		end=false;
	}
	
	@Override
    public void run() {
      try {
    	  state = p.waitFor();
    	  end = true;
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
      }
    }

	public boolean hasFinished() 
	{
		return end;
	}

	public int exitValue() 
	{
		return state;
	}
}
