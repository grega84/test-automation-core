package test.automation.core.cmd;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class ProcessUtils 
{
	public static int waitFor(final Process p, long timeout, TimeUnit unit)
		      throws TimeoutException {

		    ProcessThread t = new ProcessThread(p);
		    t.start();
		    try {
		      unit.timedJoin(t, timeout);

		    } catch (InterruptedException e) {
		      t.interrupt();
		      Thread.currentThread().interrupt();
		    }

		    if (!t.hasFinished()) {
		      throw new TimeoutException("Process did not finish within timeout");
		    }

		    return t.exitValue();
		  }
}
