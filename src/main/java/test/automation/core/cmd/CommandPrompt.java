package test.automation.core.cmd;

public class CommandPrompt 
{
	private String program;

	public CommandPrompt(String program) 
	{
		super();
		this.program = program;
	}
	
	public CommandPromptJob create(CommandArgs args)
	{
		//assert(program != null && !program.isEmpty());
		
		CommandPromptJob job=new CommandPromptJob(program,args);
		
		return job;
	}
	
}
