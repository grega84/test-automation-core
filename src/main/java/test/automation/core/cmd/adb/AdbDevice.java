package test.automation.core.cmd.adb;

public class AdbDevice 
{
	private String id;
	private String product;
	private String model;
	private String device;
	private String transportId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getTransportId() {
		return transportId;
	}
	public void setTransportId(String transportId) {
		this.transportId = transportId;
	}
	
	public String toString()
	{
		return "device product:"+this.product+" model:"+this.model+" device:"+this.device+" transport_id:"+this.transportId;
	}
}
