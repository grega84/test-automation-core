package test.automation.core.cmd.adb;

public class CurrentActivity 
{
	private String appPackage;
	private String activity;
	
	public String getAppPackage() {
		return appPackage;
	}
	public void setAppPackage(String appPackage) {
		this.appPackage = appPackage;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	
	public String toString()
	{
		return appPackage+"/"+activity;
	}
}
