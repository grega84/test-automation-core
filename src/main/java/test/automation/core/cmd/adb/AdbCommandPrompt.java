package test.automation.core.cmd.adb;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import test.automation.core.cmd.CommandArgs;
import test.automation.core.cmd.CommandPrompt;
import test.automation.core.cmd.CommandPromptJob;

public class AdbCommandPrompt 
{
	static final String quote_start="\"''";
	static final String quote_end="''\"";
	
	private String adb;
	private String deviceName;
	private CommandPrompt cmd;

	public AdbCommandPrompt(String adb,String deviceName) {
		super();
		this.adb = adb;
		this.deviceName=deviceName;
		cmd=new CommandPrompt(adb);
	}
	
	public AdbCommandPrompt(String adb) {
		super();
		this.adb = adb;
		cmd=new CommandPrompt(adb);
	}



	public HashMap<String, AdbDevice> devices()
	{
		//adb devices -l
		CommandArgs args=new CommandArgs();
		args.addArgument("devices")
		.addArgument("-l");
		
		CommandPromptJob job=cmd.create(args);
		
		job.run();
		String out=job.getOutput();
		String[] line=out.split(System.lineSeparator());
		
		assert(line.length > 0);
		assert(line[0].equals("List of devices attached"));
		
		HashMap<String, AdbDevice> devices=new HashMap<>();
		
		for(int i=1;i<line.length;i++)
		{
			String[] dev=line[i].split("device ");
			assert(dev.length == 1);
			
			if(dev.length < 2)
				continue;
			
			AdbDevice d=new AdbDevice();
			d.setId(dev[0].trim());
			String info=dev[1].replace("device ", "");
			String[] infos=info.split(" ");
			assert(infos.length >= 3);
			
			d.setProduct(infos[0].replace("product:", "").trim());
			d.setModel(infos[1].replace("model:", "").trim());
			d.setDevice(infos[2].replace("device:", "").trim());
			
			if(infos.length == 4)
				d.setTransportId(infos[3].replace("transport_id:", "").trim());
			
			devices.put(dev[0].trim(), d);
		}
		
		return devices;
	}
	
	public boolean install(String apkPath)
	{
		//adb -s emulator-5554 install "path"
		CommandArgs args=new CommandArgs();
		args.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("install")
		.addArgument("\""+apkPath+"\"");
		
		CommandPromptJob job=cmd.create(args);
		
		job.run();
		String out=job.getOutput();
		String[] line=out.split(System.lineSeparator());
		
		assert(line.length > 0);
		assert(line[0].equals("Performing Streamed Install"));
		assert(line[1].equals("Success"));
		
		return true;
	}
	
	public boolean updateApp(String apkPath)
	{
		//adb -s emulator-5554 install -r "<app>"
		CommandArgs args=new CommandArgs();
		args.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("install")
		.addArgument("-r")
		.addArgument("\""+apkPath+"\"");
		
		CommandPromptJob job=cmd.create(args);
		
		job.run();
		String out=job.getOutput();
		String[] line=out.split(System.lineSeparator());
		
		assert(line.length > 0);
		assert(line[0].equals("Performing Streamed Install"));
		assert(line[1].equals("Success"));
		
		return true;
	}
	
	public boolean uninstall(String appPackage)
	{
		//adb -s emulator-5554 uninstall "package"
		CommandArgs args=new CommandArgs();
		args.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("uninstall")
		.addArgument("\""+appPackage+"\"");
		
		CommandPromptJob job=cmd.create(args);
		
		job.run();
		String out=job.getOutput();
		String[] line=out.split(System.lineSeparator());
		
		assert(line.length > 0);
		assert(line[0].equals("Success"));
		
		return true;
	}
	
	public boolean airplaneMode(boolean enable)
	{
		//adb -s "+emuName+" shell settings put global airplane_mode_on 0
		CommandArgs args=new CommandArgs();
		args.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("shell")
		.addArgument("settings")
		.addArgument("put")
		.addArgument("global")
		.addArgument("airplane_mode_on")
		.addArgument((enable) ? "1" : "0");
		
		cmd.create(args).run();
		
		args=new CommandArgs();
		args.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("shell")
		.addArgument("settings")
		.addArgument("get")
		.addArgument("global")
		.addArgument("airplane_mode_on");
		
		CommandPromptJob job=cmd.create(args);
		job.run();
		String out=job.getOutput();
		
		if(enable)
		{
			assert(out.trim().equals("1"));
			return true;
		}
		else
		{
			assert(out.trim().equals("0"));
			return true;
		}
	}
	
	public boolean isAirplaneModeOn()
	{
		//adb -s "+emuName+" shell settings get global airplane_mode_on
		CommandArgs args=new CommandArgs();
		args=new CommandArgs();
		args.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("shell")
		.addArgument("settings")
		.addArgument("get")
		.addArgument("global")
		.addArgument("airplane_mode_on");
		
		CommandPromptJob job=cmd.create(args);
		job.run();
		String out=job.getOutput();
		
		return out.trim().equals("1");
	}
	
	public boolean wifi(boolean enable)
	{
		//adb -s "+emuName+" shell svc wifi enable\disable
		CommandArgs args=new CommandArgs();
		args.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("shell")
		.addArgument("svc")
		.addArgument("wifi")
		.addArgument((enable) ? "enable" : "disable");
		
		cmd.create(args).run();
		
		//adb shell "dumpsys wifi | grep 'Wi-Fi is'"
		args=new CommandArgs();
		args.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("shell")
		.addArgument("\"dumpsys wifi | grep 'Wi-Fi is'\"");
		
		CommandPromptJob job=cmd.create(args);
		job.run();
		String out=job.getOutput().trim();
		
		if(enable)
		{
			assert(out.equals("Wi-Fi is enabled"));
			return true;
		}
		else
		{
			assert(out.equals("Wi-Fi is disabled"));
			return true;
		}
	}
	
	public boolean isWifiEnable()
	{
		CommandArgs args=new CommandArgs();
		//adb shell "dumpsys wifi | grep 'Wi-Fi is'"
		args=new CommandArgs();
		args.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("shell")
		.addArgument("\"dumpsys wifi | grep 'Wi-Fi is'\"");
		
		CommandPromptJob job=cmd.create(args);
		job.run();
		String out=job.getOutput().trim();
		
		return out.equals("Wi-Fi is enabled");
	}
	
	public void startActivity(String activity)
	{
		//adb shell am start -n com.android.settings/.wifi.WifiSettings
		CommandArgs args=new CommandArgs();
		args.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("shell")
		.addArgument("am")
		.addArgument("start")
		.addArgument("-n")
		.addArgument(activity)
		;
		
		cmd.create(args).run();
		
	}
	
	public void forceStop(String app)
	{
		//adb shell am force-stop "com.android.settings"
		CommandArgs args=new CommandArgs();
		args.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("shell")
		.addArgument("am")
		.addArgument("force-stop")
		.addArgument("\""+app+"\"")
		;
		
		cmd.create(args).run();
	}
	
	public boolean mobileData(boolean enable)
	{
		//adb -s "+emuName+" shell svc data enable
		CommandArgs args=new CommandArgs();
		args.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("shell")
		.addArgument("svc")
		.addArgument("data")
		.addArgument((enable) ? "enable" : "disable");
		
		cmd.create(args).run();
		
		try {Thread.sleep(1000);}catch(Exception err) {}
		
		//adb shell "dumpsys telephony.registry | grep 'mDataConnectionState'"
		args=new CommandArgs();
		args.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("shell")
		.addArgument("\"dumpsys telephony.registry | grep 'mDataConnectionState'\"");
		
		CommandPromptJob job=cmd.create(args);
		job.run();
		String out=job.getOutput();
		String[] rows=out.split(System.lineSeparator());
		
		for(int i=0;i<rows.length;i++)
		{
			String[] value=rows[i].trim().split("=");
			
			if(value.length != 2)
				return false;
			
			if(enable)
			{
				if(!value[1].trim().equals("2"));
					return false;
			}
			else
			{
				if(!value[1].trim().equals("0"));
					return false;
			}
		}
		
		return true;
		
//		out=out.substring(out.indexOf("mDataConnectionState"));
//		String[] value=out.split("=");
//		
//		assert(value.length == 2);
//		
//		if(enable)
//		{
//			assert(value[1].trim().equals("2"));
//			return true;
//		}
//		else
//		{
//			assert(value[1].trim().equals("0"));
//			return true;
//		}
	}
	
	public boolean isMobileDataOn()
	{
		//adb shell "dumpsys telephony.registry | grep 'mDataConnectionState'"
		CommandArgs args=new CommandArgs();
		args=new CommandArgs();
		args.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("shell")
		.addArgument("\"dumpsys telephony.registry | grep 'mDataConnectionState'\"");
		
		CommandPromptJob job=cmd.create(args);
		job.run();
		String out=job.getOutput();
		out=out.substring(out.indexOf("mDataConnectionState"));
		String[] value=out.split("=");
		
		assert(value.length == 2);
		
		return value[1].trim().equals("2");
	}
	
	public void tap(int x,int y)
	{
		//adb -s "+emuName+" shell input tap "+pointSignInX+" "+pointSignInY
		CommandArgs args=new CommandArgs();
		args.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("shell")
		.addArgument("input")
		.addArgument("tap")
		.addArgument(""+x)
		.addArgument(""+y);
		
		cmd.create(args).run();
		
	}
	
	public void typeText(String text)
	{
		//adb -s emulator-5554 shell input text 'Wow, it so cool feature'
		
		CommandArgs args=new CommandArgs();
		args.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("shell")
		.addArgument("input")
		.addArgument("text")
		.addArgument(quote_start+escapeText(text)+quote_end);
		
		cmd.create(args).run();
	}
	
	private String escapeText(String text) 
	{
		/*
		 * //e.g. convert string "hello world\n" to string "hello%sworld\n"
    char * convert(char * trans_string)
    {
        char *s0 = replace(trans_string, '\\', "\\\\");
        free(trans_string);
        char *s = replace(s0, '%', "\\\%");
        free(s0);
        char *s1 = replace(s, ' ', "%s");//special case for SPACE
        free(s);
        char *s2 = replace(s1, '\"', "\\\"");
        free(s1);
        char *s3 = replace(s2, '\'', "\\\'");
        free(s2);
        char *s4 = replace(s3, '\(', "\\\(");
        free(s3);
        char *s5 = replace(s4, '\)', "\\\)");
        free(s4);
        char *s6 = replace(s5, '\&', "\\\&");
        free(s5);
        char *s7 = replace(s6, '\<', "\\\<");
        free(s6);
        char *s8 = replace(s7, '\>', "\\\>");
        free(s7);
        char *s9 = replace(s8, '\;', "\\\;");
        free(s8);
        char *s10 = replace(s9, '\*', "\\\*");
        free(s9);
        char *s11 = replace(s10, '\|', "\\\|");
        free(s10);
        char *s12 = replace(s11, '\~', "\\\~");
        //this if un-escaped gives current directory !
        free(s11);
    //  free(s14);
        return s14;
}
		 */
		/*
		 *    \.[]{}()<>*+-=!?^$|
		 */
		return text
				.replaceAll("%", "\\\\%")
				.replaceAll(" ", "%s")
				.replaceAll("\"", "\\\"")
				.replaceAll( "'", "\\\\'")
				.replaceAll( "\\(", "\\\\(")
				.replaceAll( "\\)", "\\\\)")
				.replaceAll( "&", "\\\\&")
				.replaceAll( "\\<", "\\\\<")
				.replaceAll( "\\>", "\\\\>")
				.replaceAll( ";", "\\\\;")
				.replaceAll( "\\*", "\\\\*")
				.replaceAll( "\\|", "\\\\|")
				.replaceAll( "~", "\\\\~")
				//.replaceAll( "�", "\\\\�")
				.replaceAll( "`", "\\\\`")
				//.replaceAll( "�", "\\\\�")
				;
	}
	
	public void swipe(int x1,int y1,int x2,int y2,int duration)
	{
		//adb shell input swipe Xpoint1 Ypoint1 Xpoint2 Ypoint2 [DURATION*]
		CommandArgs args=new CommandArgs();
		args.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("shell")
		.addArgument("input")
		.addArgument("swipe")
		.addArgument(""+x1)
		.addArgument(""+y1)
		.addArgument(""+x2)
		.addArgument(""+y2)
		.addArgument(""+duration);
		
		cmd.create(args).run();
	}
	
	public void keyEvent(AdbKeyEvent key)
	{
		//adb shell input keyevent 64
		CommandArgs args=new CommandArgs();
		args.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("shell")
		.addArgument("input")
		.addArgument("keyevent")
		.addArgument(""+key.val());
		
		cmd.create(args).run();
	}

	public boolean fingerTouch(int fingerId)
	{
		//adb -e emu finger touch fingerId
		CommandArgs args=new CommandArgs();
		args
		.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("-e")
		.addArgument("emu")
		.addArgument("finger")
		.addArgument("touch")
		.addArgument(""+fingerId);
		
		CommandPromptJob job=cmd.create(args);
		
		job.run();
		
		String out=job.getOutput();
		
		//assert(out.equals("OK"));
		
		return true;
		
	}
	
	public CurrentActivity getCurrentActivity()
	{
		//adb -s emulator-5554 shell "dumpsys window windows | grep -E 'mCurrentFocus|mFocusedApp'"
		CommandArgs args=new CommandArgs();
		args.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("shell")
		.addArgument("\"dumpsys window windows | grep -E 'mCurrentFocus|mFocusedApp'\"")
		;
		
		CommandPromptJob job=cmd.create(args);
		
		job.run();
		
		String out=job.getOutput();
		String[] line=out.split(System.lineSeparator());
		
		assert(line.length > 0);
		String window=line[0].trim()
				.replace("mCurrentFocus=", "")
				.replace("Window", "")
				.replace("{","")
				.replace("}", "");
		
		String[] W=window.split(" ");
		assert(W.length == 3);
		
		CurrentActivity activity=new CurrentActivity();
		String[] act=W[2].split("/");
		
		assert(act.length == 2);
		activity.setAppPackage(act[0]);
		activity.setActivity(act[1]);
		
		return activity;
	}
	
	public void pull(String devicePath,String localPath)
	{
		//adb -s [device] pull [device file location] [local file location]
		CommandArgs args=new CommandArgs();
		args.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("pull")
		.addArgument("\""+devicePath+"\"")
		.addArgument("\""+localPath+"\"")
		;
		
		CommandPromptJob job=cmd.create(args);
		
		job.run();
	}
	
	public void push(String localPath,String devicePath)
	{
		//adb -s [device] push [source] [destination]
		CommandArgs args=new CommandArgs();
		args.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("push")
		.addArgument("\""+localPath+"\"")
		.addArgument("\""+devicePath+"\"")
		;
		
		CommandPromptJob job=cmd.create(args);
		
		job.run();
	}
	
	public boolean remove(String devicePath)
	{
		//adb -s emulator-5554 shell rm -r "/sdcard/screenshot.png"
		CommandArgs args=new CommandArgs();
		args.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("shell")
		.addArgument("rm")
		.addArgument("-rf")
		.addArgument("\""+devicePath+"\"")
		;
		
		CommandPromptJob job=cmd.create(args);
		
		job.run();
		
		String out=job.getOutput();
		
		assert(!out.contains("No such file or directory"));
		
		args=new CommandArgs();
		args.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("shell")
		.addArgument("ls")
		.addArgument("\""+devicePath+"\"")
		;
		
		job=cmd.create(args);
		
		try
		{
			job.run();
		}
		catch(Exception err)
		{
			
		}
		
		out=job.getOutput();
		assert(out.contains("No such file or directory"));
		
		return true;
	}
	
	public void screenShot(String localPath)
	{
		//adb -s emulator-5554 shell screencap -p /sdcard/screenshot.png
		SimpleDateFormat format=new SimpleDateFormat("yyyyMMdd_HHmmss");
		Calendar c=Calendar.getInstance();
		String date=format.format(c.getTime());
		String fileName="/sdcard/screen_"+date+".png";
		
		CommandArgs args=new CommandArgs();
		args.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("shell")
		.addArgument("screencap")
		.addArgument("-p")
		.addArgument("\""+fileName+"\"")
		;
		
		CommandPromptJob job=cmd.create(args);
		
		job.run();
		
		this.pull(fileName, localPath);
		
		this.remove(fileName);
	}
	
	public String getSoftwareVersion(String appPackage)
	{
		//adb -s [device] shell "dumpsys package posteitaliane.posteapp.appbpol | grep versionName | ( read h; echo ${h:12} )"
		CommandArgs args=new CommandArgs();
		args.addArgument("-s")
		.addArgument(deviceName)
		.addArgument("shell")
		.addArgument("\"dumpsys package ")
		.addArgument(appPackage)
		.addArgument(" | grep versionName | ( read h; echo ${h:12} )\"")
		;
		
		CommandPromptJob job=cmd.create(args);
		
		job.run();
		
		return job.getOutput().trim();
	}
	
	public void disableGps()
	{
		try {
			CommandArgs args=new CommandArgs();
			args.addArgument("-s")
			.addArgument(deviceName)
			.addArgument("shell")
			.addArgument("settings")
			.addArgument("put")
			.addArgument("secure")
			.addArgument("location_mode")
			.addArgument("0")
			;
			
			CommandPromptJob job=cmd.create(args);
			
			job.run();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			CommandArgs args=new CommandArgs();
			args.addArgument("-s")
			.addArgument(deviceName)
			.addArgument("shell")
			.addArgument("settings")
			.addArgument("put")
			.addArgument("secure")
			.addArgument("location_providers_allowed")
			.addArgument("-gps")
			;
			
			CommandPromptJob job=cmd.create(args);
			
			job.run();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			CommandArgs args=new CommandArgs();
			args.addArgument("-s")
			.addArgument(deviceName)
			.addArgument("shell")
			.addArgument("settings")
			.addArgument("put")
			.addArgument("secure")
			.addArgument("location_providers_allowed")
			.addArgument("-network")
			;
			
			CommandPromptJob job=cmd.create(args);
			
			job.run();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void enableGps()
	{
		try {
			CommandArgs args=new CommandArgs();
			args.addArgument("-s")
			.addArgument(deviceName)
			.addArgument("shell")
			.addArgument("settings")
			.addArgument("put")
			.addArgument("secure")
			.addArgument("location_mode")
			.addArgument("3")
			;
			
			CommandPromptJob job=cmd.create(args);
			
			job.run();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			CommandArgs args=new CommandArgs();
			args.addArgument("-s")
			.addArgument(deviceName)
			.addArgument("shell")
			.addArgument("settings")
			.addArgument("put")
			.addArgument("secure")
			.addArgument("location_providers_allowed")
			.addArgument("+gps")
			;
			
			CommandPromptJob job=cmd.create(args);
			
			job.run();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			CommandArgs args=new CommandArgs();
			args.addArgument("-s")
			.addArgument(deviceName)
			.addArgument("shell")
			.addArgument("settings")
			.addArgument("put")
			.addArgument("secure")
			.addArgument("location_providers_allowed")
			.addArgument("+network")
			;
			
			CommandPromptJob job=cmd.create(args);
			
			job.run();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	
	
}
