package test.automation.core.cmd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.Buffer;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;



public class CommandPromptJob implements Runnable
{
	private static final int BUF_SIZE = 0x800; // 2K chars (4K bytes)
	protected static final Logger logger = LogManager.getLogger(CommandPromptJob.class);
	
	public enum State {
		NOTSTARTED,
	    RUNNING,
	    FINISHED,
	    FAILED,
	  }
	
	private State state;
	private CommandArgs args;
	private String program;
	private String output;
	
	public CommandPromptJob(String program, CommandArgs args) 
	{
		super();
		this.program=program;
		this.args = args;
		this.state=State.NOTSTARTED;
	}



	@Override
	public void run() 
	{
		this.state=State.RUNNING;
		logger.info("Process "+State.RUNNING);
		Process p=null;
		
		try
		{
			args.addProgram(program);
			List<String> _args=args.getArgs();
			String command=String.join(" ", _args);
			logger.info("start of command "+command);
			System.out.println("start of command "+command);
			
			ProcessBuilder builder = new ProcessBuilder(_args);
			builder.redirectErrorStream(true);
			p=builder.start();
			StringBuffer buff=new StringBuffer();
			
			BufferedReader reader=new BufferedReader(new InputStreamReader(p.getInputStream(), StandardCharsets.UTF_8));
			
			CharBuffer buf = CharBuffer.allocate(BUF_SIZE);
		    long total = 0;
		    
		    while (reader.read(buf) != -1) 
		    {
		      ((Buffer)buf).flip();
		      buff.append(buf);
		      total += ((Buffer)buf).remaining();
		      ((Buffer)buf).clear();
		      System.out.print(((Buffer)buf).toString());
		    }
		    
		    logger.info("readed "+total+" bytes");
			
			try
			{
				int exitCode=ProcessUtils.waitFor(p, 1, TimeUnit.SECONDS);
				
				if(exitCode != 0)
				{
					logger.error("command ends with error "+buff.toString());
					throw new Exception("command ends with error "+buff.toString());
				}
			}
			catch(Exception err)
			{
				throw new IOException("Timed out waiting for " + command + " to finish.");
			}
			
			this.output=buff.toString();
			
			this.state=State.FINISHED;
		}
		catch(Exception err)
		{
			this.state=State.FAILED;
			logger.error("Process "+State.RUNNING);
			throw new RuntimeException(err);
		}
		finally 
		{
			if(p != null)
				p.destroy();
	    }
	}
	
	public State getState()
	{
		return state;
	}
	
	public String getOutput()
	{
		return output;
	}
}
