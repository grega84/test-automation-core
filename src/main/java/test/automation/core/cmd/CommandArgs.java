package test.automation.core.cmd;

import java.util.ArrayList;
import java.util.LinkedList;

public class CommandArgs 
{
	private LinkedList<String> args;
	
	public CommandArgs() 
	{
		args=new LinkedList<>();
	}
	
	public CommandArgs addArgument(String arg)
	{
		//assert(arg != null);
		this.args.add(arg);
		return this;
	}
	
	public void addProgram(String proc)
	{
		args.addFirst(proc);
	}

	public LinkedList<String> getArgs() {
		return args;
	}
}
