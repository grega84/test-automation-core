package test.automation.core;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.junit.Assert;
import org.opencv.core.Core;
import org.opencv.core.Core.MinMaxLocResult;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.openqa.selenium.Rectangle;


import nu.pattern.OpenCV;

public class ImageUtils 
{
	private Mat source;
	private Mat template;
	private double prox;
	private int x,y,w,h;
	private Robot robot;
	
	private static ImageUtils singleton;
	
	public static ImageUtils image()
	{
		if(singleton == null)
			singleton=new ImageUtils();
		
		return singleton;
	}

	private ImageUtils()
	{
		OpenCV.loadShared();
		
		try {
			robot=new Robot();
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Rectangle findImageAndReturnBounds(String imageFileToFind,String imageFileScreen,double maxThreshold)
	{
		find(imageFileToFind,imageFileScreen);
		
		return (prox >= maxThreshold) ? new Rectangle(x, y, w, h) : null;
	}
	
	public boolean findImage(String imageFileToFind,String imageFileScreen,double maxThreshold)
	{
		//matchingMethod4(screen,toFind);
		//Load image file
        find(imageFileToFind,imageFileScreen);
        
        return prox >= maxThreshold;
	}
	
	public void visibilityOfImage(String imageToFind,double maxThreshold,int x,int y,int width,int height,TimeUnit unit,long time) throws Exception
	{
		java.awt.Rectangle rectangle=new java.awt.Rectangle(x,y,width,height);
		visibility(rectangle, imageToFind, maxThreshold, unit, time);
	}
	
	private void visibility(java.awt.Rectangle rectangle,String imageToFind,double maxThreshold,TimeUnit unit,long time) throws Exception 
	{
		long toTime=unit.toMillis(time);
		
		BufferedImage screen=robot.createScreenCapture(rectangle);
		File temp=null;
		try {
			temp = File.createTempFile("temp-screen-captured", ".png");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Assert.assertTrue(temp != null);
		
		try 
		{
			ImageIO.write(screen, "PNG", temp);
			
			while(toTime >= 0)
			{
				if(findImage(imageToFind, temp.getAbsolutePath(), maxThreshold))
				{
					return;
				}
				
				Thread.sleep(500);
				
				toTime-=500;
				System.out.println("retrying to find image "+imageToFind);
				
				screen=robot.createScreenCapture(rectangle);
				ImageIO.write(screen, "PNG", temp);
			}
			
			throw new Exception("wait for "+unit+" "+time+" until "+imageToFind+" is visible");
			
		} catch (Exception e) 
		{
			throw e;
		}
		
	}

	public void visibilityOfImage(String imageToFind,double maxThreshold,TimeUnit unit,long time) throws Exception
	{
		Dimension dimension=Toolkit.getDefaultToolkit().getScreenSize();
		java.awt.Rectangle rectangle=new java.awt.Rectangle(dimension);
		
		visibility(rectangle, imageToFind, maxThreshold, unit, time);
		
	}

	private void find(String imageFileToFind, String imageFileScreen) 
	{
		prox=0;
		x=y=w=h=0;
		
		source=Imgcodecs.imread(imageFileScreen,Imgcodecs.CV_LOAD_IMAGE_COLOR);
        template=Imgcodecs.imread(imageFileToFind,Imgcodecs.CV_LOAD_IMAGE_COLOR);
        
        int result_cols =  source.cols() - template.cols() + 1;
        int result_rows = source.rows() - template.rows() + 1;
        
        Mat outputImage=new Mat(result_rows,result_cols,CvType.CV_32FC1);    
        int machMethod=Imgproc.TM_CCOEFF_NORMED;
        //Template matching method
        Imgproc.matchTemplate(source, template, outputImage, machMethod,new Mat());
        //Core.normalize(outputImage, outputImage, 0, 1, Core.NORM_MINMAX, -1, new Mat());
        
        MinMaxLocResult mmr = Core.minMaxLoc(outputImage);
        Point matchLoc=mmr.maxLoc;
        
        prox=mmr.maxVal;
        
        x=(int) matchLoc.x;
        y=(int) matchLoc.y;
        w=template.rows();
        h=template.cols();
        
        /*
        if( machMethod  == Imgproc.TM_SQDIFF || machMethod == Imgproc.TM_SQDIFF_NORMED )
        { 
        	matchLoc = mmr.minLoc;
        	prox=mmr.minVal;
        }
        else
        { 
        	matchLoc = mmr.maxLoc;
        	prox=mmr.maxVal;
        }

        
        //Draw rectangle on result image
        Imgproc.rectangle(source, matchLoc, new Point(matchLoc.x + template.cols(),
                matchLoc.y + template.rows()), new Scalar(0, 0, 255));
 
        Imgcodecs.imwrite("C:\\Users\\Francesco\\Pictures\\stocazz.jpg", source);
        */
        /*
        System.out.println("MinVal "+ mmr.minVal);
        System.out.println("MaxVal "+ mmr.maxVal);
        System.out.println("prox:"+prox);

        System.out.println("xVal "+ matchLoc.x);
        System.out.println("yVal "+ matchLoc.y);
        */
	}

	public static void main(String[] argv)
	{
		boolean find=ImageUtils.image().findImage("C:\\Users\\Francesco\\Pictures\\toFind.png", "C:\\Users\\Francesco\\Pictures\\screenshoot.png",0.75);
		System.out.println("find:"+find);
		find=ImageUtils.image().findImage("C:\\Users\\Francesco\\Pictures\\Cattura.PNG","C:\\Users\\Francesco\\Pictures\\screenshoot.png",0.75);
		System.out.println("find:"+find);
		
		Rectangle bound=ImageUtils.image().findImageAndReturnBounds("C:\\Users\\Francesco\\Pictures\\toFind.png", "C:\\Users\\Francesco\\Pictures\\screenshoot.png",0.75);
		System.out.println("topLeft(x:"+bound.x+",y:"+bound.y+")");
		System.out.println("bottomRight(x:"+(bound.x+bound.width)+",y:"+(bound.y+bound.height)+")");
		
		System.out.println("visibility of image");
		try {
			ImageUtils.image().visibilityOfImage("C:\\Users\\Francesco\\Pictures\\toFind.png", 0.75, TimeUnit.SECONDS, 5);
			System.out.println("finded");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
