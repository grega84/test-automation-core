package test.automation.core;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import test.automation.core.benchmark.BenchmarkableAnnotationObject;
import test.automation.core.benchmark.BenchmarkableContext;
import test.automation.core.benchmark.UiBenchmarkableContext;
import test.automation.core.har.HarFileUtils;
import test.automation.core.properties.FileMultiPropertiesLoaderImpl;
import test.automation.core.properties.MultiPropertiesLoader;
import test.automation.core.videorecorder.VideoRecorder;
import test.automation.core.videorecorder.mobile.MobileVideoRecorder;

public class UIUtils {

	private static final Logger LOGGER = LogManager.getLogger(UIUtils.class);

	public static final String DRIVER_TARGET_URL = "driver.target.url";

	/**
	 * Driver type. [firefox|chrome|iexplorer|electron|mobile]
	 */
	public static final String DRIVER_TYPE = "driver.type";

	public static final String DRIVER_TYPE_CHROME = "chrome";
	public static final String DRIVER_TYPE_FIREFOX = "firefox";
	public static final String DRIVER_TYPE_ELECTRON = "electron";
	public static final String DRIVER_TYPE_IEXPLORER = "iexplorer";
	public static final String DRIVER_TYPE_MOBILE = "mobile";
	private static final String DRIVER_TYPE_SAFARI = "safari";
	public static final String DRIVER_TYPE_REMOTE = "remote";
	public static final String CHROME_DRIVER = "chrome.driver";
	public static final String IE_DRIVER = "ie.driver";
	public static final String FIREFOX_DRIVER="webdriver.gecko.driver";
	public static final String ELECTRON_BINARY = "electron.binary";
	public static final String ELECTRON_APP_ARGS = "electron.app.args";
	public static final String REPORT_IMAGES_DIR = "report.images.dir";
	public static final String DEFAULT_REPORT_IMAGES_DIR = "embedded-images";
	public static final String SCENARIO_ID = "scenario.calculated.id";
	public static enum SCROLL_DIRECTION {UP, DOWN, LEFT, RIGHT};

	// android properties specific
	public static final String BROWSER_NAME = "browser.name";
	public static final String ANDROID_FULL_RESET_APP = "android.fullreset";
	public static final String ANDROID_PLATFORM_VERSION = "android.platform.version";
	public static final String ANDROID_DEVICE_NAME = "android.device.name";
	public static final String ANDROID_APP_PACKAGE = "android.app.package";
	public static final String ANDROID_APP_ACTIVITY = "android.app.activity";
	public static final String ANDROID_SERVER_URL = "android.server.url";
	public static final String ANDROID_NEW_COMMAND_TIMEOUT = "android.new.command.timeout";
	public static final String IOS_BUNDLE_ID = "ios.bundle.id";
	public static final String IOS_UDID = "ios.udid";
	public static final String DEVICE_OS="device.os";
	public static final String APPIUM_SYSTEM_PORT="appium.system.port";
	public static final String APPIUM_MJPEG_SERVER_PORT = "android.mjpegServerPort";
	private static final String ADDITIONAL_CAPABILITY = "capability.";
	private static final String IOS_START_IWDP = "startIWDP";
	private static final String IOS_FULL_CONTEXT_LIST = "fullContextList";



	public static final String ANDROID_NO_RESET = "android.noReset";
	public static final String ANDROID_AUTOMATION_NAME="android.automation.name";
	// properties for grid configuration
	public static final String GRID_SERVER_URL = "grid.server.url";
	public static final String GRID_BROWSER_TYPE = "grid.browser.type";
	public static final String GRID_BROWSER_VERSION = "grid.browser.version";
	public static final String GRID_PLATFORM_TYPE = "grid.platform.type";

	// Fix Sonar critical issue
	private static final long DEFAULT_EXPLICT_WAIT_TIMEOUT = 10L;
	private static final long DEFAULT_IMPLICIT_WAIT_TIMEOUT = 10L;

	private static final String SCROLL_TO_ELEMENT = "arguments[0].scrollIntoView({behavior: \"smooth\",block: \"end\",inline: \"end\"});";

	private static final String VIDEO_FORMAT = "video.format";

	private static final String VIDEO_FRAMERATE = "video.framerate";

	private static final String VIDEO_FOLDER = "video";

	private static final String REPORT_VIDEO_DIR = "report.images.dir";

	private static final String DEFAULT_REPORT_VIDEO_DIR = "embedded-videos";

	private static final String SCROLL_TO_ELEMENT_IE = "Element.prototype.documentOffsetTop = function () {\r\n" + 
			"    return this.offsetTop + ( this.offsetParent ? this.offsetParent.documentOffsetTop() : 0 );\r\n" + 
			"};\r\n" + 
			"\r\n" + 
			"var top = arguments[0].documentOffsetTop() - ( window.innerHeight / 2 );\r\n" + 
			"window.scrollTo( 0, top );";

	private static final String APP_ACTIVITY = "appActivity";




	private static UIUtils instance;

	private Map<String, Properties> propertiesBundles = null;

	protected WebDriver currentDriver = null;

	protected VideoRecorder video = null;

	protected MobileVideoRecorder mobileVideo= null;

	protected MobileUtils mobile=null;

	protected SafariUtils safari=null;

	private Properties p = null;

	private UIUtils() {
		MultiPropertiesLoader multiPropertiesLoader = new FileMultiPropertiesLoaderImpl(
				(file) -> file.getName().split("-")[0]);

		propertiesBundles = multiPropertiesLoader.loadAndCheck((d, name) -> name.endsWith("-ui.properties"),
				DRIVER_TYPE);
	}

	public synchronized static UIUtils ui() {
		if (instance == null) {
			instance = new UIUtils();
		}
		return instance;
	}

	public synchronized static MobileUtils mobile()
	{
		if(instance.mobile == null)
			instance.mobile=new MobileUtils();

		return instance.mobile;
	}

	public synchronized static SafariUtils safari()
	{
		if(instance.safari == null)
			instance.safari=new SafariUtils();

		return instance.safari;
	}

	/**
	 * Build a SeleniumWebDriver configured using
	 * <configurationType>-ui.properties If the properties 'driver.target.url'
	 * is set, the driver will point to that URL.
	 * 
	 * @param configurationType
	 * @return the Selenium WebDriver configured. The implicit wait is 10
	 *         seconds.
	 */
	public WebDriver driver(String configurationType) {
		this.currentDriver = buildDriverWith(configurationType);
		return currentDriver;
	}

	/**
	 * Build a SeleniumWebDriver configured using default-ui.properties
	 *
	 * @return the Selenium WebDriver configured
	 */
	public WebDriver driver() {
		return driver("default");
	}

	public String mobileVideoRecorder(String fileName,String propertiesFile)
	{
		if(mobileVideo == null)
			mobileVideo= new MobileVideoRecorder();

		//video.stop();
		p= propertiesBundles.get(propertiesFile);

		String workingDir = System.getProperty("user.dir");
		StringBuilder sb = new StringBuilder();
		String reportImageDir = p.getProperty(REPORT_VIDEO_DIR, DEFAULT_REPORT_VIDEO_DIR);
		sb.append(workingDir).append(File.separatorChar).append("target").append(File.separatorChar)
		.append(reportImageDir);
		try {
			FileUtils.forceMkdir(new File(sb.toString()));
			System.setProperty(REPORT_VIDEO_DIR, sb.toString());
		} catch (IOException e) {
			throw new RuntimeException("Unable to create report image dir " + sb.toString());
		}

		SimpleDateFormat format=new SimpleDateFormat("yyyyMMdd_HHmmss");

		String fileNameAbsolutePath = sb.toString() + File.separatorChar + fileName+"_"+format.format(Calendar.getInstance().getTime())+".mp4";

		System.out.println("#### init mobile video recorder ####");
		System.out.println("video path:"+fileNameAbsolutePath);

		mobileVideo.init(fileNameAbsolutePath);

		return fileNameAbsolutePath;
	}

	public String videoRecoder(String fileName, String prop)
	{
		if(video == null)
			video= new VideoRecorder();

		//video.stop();
		p= propertiesBundles.get(StringUtils.isBlank(prop) ? "default" : prop);

		String workingDir = System.getProperty("user.dir");
		StringBuilder sb = new StringBuilder();
		String reportImageDir = p.getProperty(REPORT_VIDEO_DIR, DEFAULT_REPORT_VIDEO_DIR);
		sb.append(workingDir).append(File.separatorChar).append("target").append(File.separatorChar)
		.append(reportImageDir);
		try {
			FileUtils.forceMkdir(new File(sb.toString()));
			System.setProperty(REPORT_VIDEO_DIR, sb.toString());
		} catch (IOException e) {
			throw new RuntimeException("Unable to create report image dir " + sb.toString());
		}

		SimpleDateFormat format=new SimpleDateFormat("yyyyMMdd_hhmmss");

		//String fileNameAbsolutePath = sb.toString() + File.separatorChar + fileName+"_"+format.format(Calendar.getInstance().getTime())+".mp4";
		String fileNameAbsolutePath = sb.toString() + File.separatorChar + fileName;

		System.out.println("#### init video recorder ####");
		System.out.println("video path:"+fileNameAbsolutePath);
		video.setScreenId(-1);
		video.init(fileNameAbsolutePath);

		return fileNameAbsolutePath;
	}

	public String videoRecoder(int screenId,String fileName, String prop)
	{
		if(video == null)
			video= new VideoRecorder();

		//video.stop();
		p= propertiesBundles.get(StringUtils.isBlank(prop) ? "default" : prop);

		String workingDir = System.getProperty("user.dir");
		StringBuilder sb = new StringBuilder();
		String reportImageDir = p.getProperty(REPORT_VIDEO_DIR, DEFAULT_REPORT_VIDEO_DIR);
		sb.append(workingDir).append(File.separatorChar).append("target").append(File.separatorChar)
		.append(reportImageDir);
		try {
			FileUtils.forceMkdir(new File(sb.toString()));
			System.setProperty(REPORT_VIDEO_DIR, sb.toString());
		} catch (IOException e) {
			throw new RuntimeException("Unable to create report image dir " + sb.toString());
		}

		SimpleDateFormat format=new SimpleDateFormat("yyyyMMdd_hhmmss");

		//String fileNameAbsolutePath = sb.toString() + File.separatorChar + fileName+"_"+format.format(Calendar.getInstance().getTime())+".mp4";
		String fileNameAbsolutePath = sb.toString() + File.separatorChar + fileName;

		System.out.println("#### init video recorder ####");
		System.out.println("video path:"+fileNameAbsolutePath);

		video.setScreenId(screenId);
		video.init(fileNameAbsolutePath);

		return fileNameAbsolutePath;
	}

	public void startVideoRecording()
	{
		try
		{
			if(video != null)
				video.start();
		}
		catch(Exception er)
		{

		}
	}

	public void startMobileVideoRecording(AppiumDriver<?> driver)
	{
		try
		{
			System.out.println("#### start mobile video recorder ####");
			if(mobileVideo != null)
				mobileVideo.start(driver);
		}
		catch(Exception er)
		{
			er.printStackTrace();
		}
	}

	public void stopVideoRecording()
	{
		try
		{
			if(video != null)
				video.stop();
		}
		catch(Exception er)
		{

		}
	}

	public void stopMobileVideoRecording()
	{
		try
		{
			System.out.println("#### stop mobile video recorder ####");
			if(mobileVideo != null)
				mobileVideo.stop();
		}
		catch(Exception er)
		{

		}
	}

	public String makeVideoEmbendedDiv(String videoPath)
	{
		return "<video width=\"640\" controls>\r\n" + 
				"  <source src=\""+ videoPath + "\" type=\"video/mp4\">\r\n" + 
				"  Your browser does not support HTML5 video.\r\n" + 
				"</video>";
	}

	private void createScreenshotDir(Properties p) {
		String workingDir = System.getProperty("user.dir");
		StringBuilder sb = new StringBuilder();
		String reportImageDir = p.getProperty(REPORT_IMAGES_DIR, DEFAULT_REPORT_IMAGES_DIR);
		sb.append(workingDir).append(File.separatorChar).append("target").append(File.separatorChar)
		.append(reportImageDir);
		try {
			FileUtils.forceMkdir(new File(sb.toString()));
			System.setProperty(REPORT_IMAGES_DIR, sb.toString());
		} catch (IOException e) {
			throw new RuntimeException("Unable to create report image dir " + sb.toString());
		}
	}

	/**
	 * Quits this driver, closing every associated window.
	 */
	public void closeDriver(WebDriver driver) {

		BenchmarkableContext context = AutomationThreadLocal.get();
		if (driver != null) {
			if (context != null) {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("dynaTrace.endVisit();");

			}

			driver.quit();
		}

	}

	/**
	 * scroll to an element located by
	 */

	public void scrollToElementLocatedBy(WebDriver driver,By locator)
	{
		JavascriptExecutor jse = (JavascriptExecutor) driver;

		if(!(driver instanceof InternetExplorerDriver))
			jse.executeScript(SCROLL_TO_ELEMENT, driver.findElement(locator));
		else
		{
			jse.executeScript(SCROLL_TO_ELEMENT_IE,driver.findElement(locator));
		}
	}

	public WebElement scrollAndReturnElementLocatedBy(WebDriver driver,By locator)
	{
		JavascriptExecutor jse = (JavascriptExecutor) driver;

		if(!(driver instanceof InternetExplorerDriver))
			jse.executeScript(SCROLL_TO_ELEMENT, driver.findElement(locator));
		else
		{
			jse.executeScript(SCROLL_TO_ELEMENT_IE,driver.findElement(locator));
		}

		try {Thread.sleep(500);} catch(Exception err) {}

		return waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(locator));
	}

	public void scroll(WebDriver driver, SCROLL_DIRECTION direction, int pixel) {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		switch (direction) {
		case UP:
			jse.executeScript("window.scrollBy(0,-"+ pixel + ")");
			break;
		case DOWN:
			jse.executeScript("window.scrollBy(0,"+ pixel + ")");
			break;
		case LEFT:
			jse.executeScript("window.scrollBy(-"+ pixel+",0)");
			break;
		case RIGHT:
			jse.executeScript("window.scrollBy("+ pixel+",0)");
			break;

		}

	}

	/**
	 * Wait for a function evaluation using this time in second Method usage:
	 * 
	 * <pre>
	 * WebElement element = UIUtils.ui().waitForElement(driver,
	 * 		ExpectedConditions.visibilityOfElementLocated(By.id("someid")), 10);
	 * </pre>
	 * 
	 * @param driver
	 * @param expectedCondition
	 *            ExpectedCondition for that element
	 * @return T The function's expected return type.
	 */
	public <T> T waitForCondition(WebDriver driver, ExpectedCondition<T> expectedCondition) {
		return waitForCondition(driver, expectedCondition, DEFAULT_EXPLICT_WAIT_TIMEOUT);
	}

	/**
	 * Wait for an element using this time in second Method usage:
	 * 
	 * <pre>
	 * WebElement element = UIUtils.ui().waitForElement(driver,
	 * 		ExpectedConditions.visibilityOfElementLocated(By.id("someid")), 10);
	 * </pre>
	 * 
	 * @param driver
	 * @param expectedCondition
	 *            ExpectedCondition for that element
	 * @param timeout
	 *            timeout in second
	 * @return T The function's expected return type.
	 */
	public <T> T waitForCondition(WebDriver driver, ExpectedCondition<T> expectedCondition, long timeout) 
	{

		WebDriverWait wait = new WebDriverWait(driver, timeout);
		//T value = wait.until((Function<? super WebDriver, T>) expectedCondition);
		return wait.until((Function<? super WebDriver, T>) expectedCondition);

		/*
		FluentWait wait=new FluentWait(driver);

		wait.withTimeout(timeout, TimeUnit.SECONDS);

		wait.pollingEvery(200, TimeUnit.MILLISECONDS);

		wait.ignoring(NoSuchElementException.class);

		return (T) wait.until((Function) expectedCondition);*/

	}

	private WebDriver buildDriverWith(String configurationType) {
		p = propertiesBundles.get(configurationType);

		if (p == null) {
			throw new IllegalArgumentException("Unable to find configuration for type: " + configurationType);
		}

		createScreenshotDir(p);
		String driverType = p.getProperty(DRIVER_TYPE);

		// get har.file.enabled property for the built driver

		WebDriver driverBuilt = null;
		if (StringUtils.equalsIgnoreCase(driverType, DRIVER_TYPE_FIREFOX)) {
			driverBuilt = buildFirefoxDriver(p);
		} else if (StringUtils.equalsIgnoreCase(driverType, DRIVER_TYPE_CHROME)) {
			driverBuilt = buildChromeDriver(p);
		} else if (StringUtils.equalsIgnoreCase(driverType, DRIVER_TYPE_IEXPLORER)) {
			driverBuilt = buildIEDriver(p);
		} else if (StringUtils.equalsIgnoreCase(driverType, DRIVER_TYPE_ELECTRON)) {
			driverBuilt = buildElectronDriver(p);
		} else if (StringUtils.equalsIgnoreCase(driverType, DRIVER_TYPE_MOBILE)) {
			driverBuilt = buildAndroidDriver(p);
		} else if (StringUtils.equalsIgnoreCase(driverType, DRIVER_TYPE_SAFARI))
		{
			driverBuilt= buildSafariDriver(p);
		}
		else if (StringUtils.equalsIgnoreCase(driverType, DRIVER_TYPE_REMOTE)) {
			try {
				driverBuilt = buildRemoteDriver(p);
			} catch (MalformedURLException e) {
				throw new TestConfigurationException(
						"A malformed URL has occurred configuring test property for grid server url!", e);
			}
		} else {
			throw new UnsupportedOperationException("Unsupported driver type: " + driverType);
		}

		// set the browser url to target URL for driver type not equals to
		// mobile
		//if (!StringUtils.equalsIgnoreCase(driverType, DRIVER_TYPE_MOBILE)) {
		String targetURL = p.getProperty(DRIVER_TARGET_URL);
		if (StringUtils.isNotBlank(targetURL)) {
			driverBuilt.get(targetURL);
		}
		//}

		performUiMonitoring(driverBuilt);
		driverBuilt.manage().timeouts().implicitlyWait(DEFAULT_IMPLICIT_WAIT_TIMEOUT, TimeUnit.SECONDS);

		return driverBuilt;

	}

	private WebDriver buildSafariDriver(Properties p) 
	{
		return new SafariDriver();
	}

	private void performUiMonitoring(WebDriver driverBuilt) {
		UiBenchmarkableContext context = (UiBenchmarkableContext) AutomationThreadLocal.get();
		if (context != null) {
			List<BenchmarkableAnnotationObject> benchmarkableObjectList = context.getBenchmarkableObjectList();
			JavascriptExecutor js = (JavascriptExecutor) driverBuilt;

			BenchmarkableAnnotationObject benchmarkableObject = benchmarkableObjectList.get(0);
			String testRunId = context.getTestRunId();

			js.executeScript("sessionStorage.DT_TESTRUNID = \"" + testRunId + "\";");
			js.executeScript("sessionStorage.DT_TESTNAME = \"" + benchmarkableObject.getName() + "\";");

			context.setDriver(driverBuilt);
			AutomationThreadLocal.unset();
			AutomationThreadLocal.set(context);

		}
	}

	private WebDriver buildAndroidDriver(Properties p) {
		// build an AndroidDriver using a capabilities
		DesiredCapabilities caps = new DesiredCapabilities();

		String platformVersion = p.getProperty(ANDROID_PLATFORM_VERSION);
		if(StringUtils.isNotBlank(platformVersion)){

			caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, platformVersion);
		}
		String deviceOs = p.getProperty(DEVICE_OS);
//		if(StringUtils.isNotBlank(deviceOs)){
//
//			caps.setCapability("device.os", deviceOs);
//		}
		String deviceName = p.getProperty(ANDROID_DEVICE_NAME);
		if(StringUtils.isNotBlank(deviceName)){


			caps.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
		}
		// Move directly into Webview context allowing you to avoid context
		// switch for Hybrid mobile app testing. Default false.
		//		caps.setCapability(MobileCapabilityType.AUTO_WEBVIEW, Boolean.TRUE);

		String appPackage = p.getProperty(ANDROID_APP_PACKAGE);
		if(StringUtils.isNotBlank(appPackage)){


			caps.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, appPackage);
		}
		String appActivity = p.getProperty(ANDROID_APP_ACTIVITY);
		if(StringUtils.isNotBlank(appActivity)){

			caps.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, appActivity);
		}

		String browserName=p.getProperty(BROWSER_NAME);
		if(StringUtils.isNotBlank(browserName)){
			caps.setCapability(MobileCapabilityType.BROWSER_NAME, browserName);
		}

		String fullResetApp= p.getProperty(ANDROID_FULL_RESET_APP);
		if(StringUtils.isNotBlank(fullResetApp)){
			caps.setCapability(MobileCapabilityType.FULL_RESET, fullResetApp);
		}
		// this property is not mandatory
		String chromeDriverMobile = p.getProperty(CHROME_DRIVER);
		if (StringUtils.isNotBlank(chromeDriverMobile)) {
			caps.setCapability(AndroidMobileCapabilityType.CHROMEDRIVER_EXECUTABLE, chromeDriverMobile);
		}
		String newBundleId = p.getProperty(IOS_BUNDLE_ID);
		if (StringUtils.isNotBlank(newBundleId)) {
			caps.setCapability(IOSMobileCapabilityType.BUNDLE_ID, newBundleId);
		}
		String newUdId = p.getProperty(IOS_UDID);
		if (StringUtils.isNotBlank(newUdId)) {
			caps.setCapability(MobileCapabilityType.UDID, newUdId);
		}
		// Not mandatory property to define how long Appium will wait for a new
		// command before ending the session
		String newCommandTimeOut = p.getProperty(ANDROID_NEW_COMMAND_TIMEOUT);
		if (StringUtils.isNotBlank(newCommandTimeOut)) {
			caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, Integer.parseInt(newCommandTimeOut));
		}
		String newAutomationName = p.getProperty(ANDROID_AUTOMATION_NAME);
		if (StringUtils.isNotBlank(newAutomationName)) {
			caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, newAutomationName);
		}
		//		String newNoReset = p.getProperty(ANDROID_NO_RESET);
		//		if(StringUtils.isNotBlank(newNoReset)){
		//			caps.setCapability(MobileCapabilityType.NO_RESET, newNoReset);
		//		}

		String newNoReset = p.getProperty(ANDROID_NO_RESET);
		if(StringUtils.isNotBlank(newNoReset)){
			caps.setCapability(MobileCapabilityType.NO_RESET, newNoReset.equals("true") ? true : false);
		}

		String newAppiumPort = p.getProperty(APPIUM_SYSTEM_PORT);
		if (StringUtils.isNotBlank(newAppiumPort)) {
			caps.setCapability(AndroidMobileCapabilityType.SYSTEM_PORT, newAppiumPort);
		}

		String newmjpegServerPort = p.getProperty(APPIUM_MJPEG_SERVER_PORT);
		if (StringUtils.isNotBlank(newmjpegServerPort)) {
			caps.setCapability("mjpegServerPort", newmjpegServerPort);
		}

		//SOLO IOS
		String startIWDP = p.getProperty(IOS_START_IWDP);
		if (StringUtils.isNotBlank(startIWDP)) {
			caps.setCapability("startIWDP", startIWDP.equals("true"));
		}

		//SOLO IOS
		String fullContextList = p.getProperty(IOS_FULL_CONTEXT_LIST);
		if (StringUtils.isNotBlank(fullContextList)) {
			caps.setCapability("fullContextList", fullContextList.equals("true"));
		}

		//Additional Capabilities
		for(Object c : p.keySet())
		{
			String k=(String) c;
			
			if(k.startsWith(ADDITIONAL_CAPABILITY))
			{
				//struttura rispetto alla nuova RC 
				//https://taa-automation.myjetbrains.com/youtrack/articles/TAA-A-420/Device-Configuration-File
				//capability.appiumSeleniumCapabilityName.typeOfValue = value
				System.out.println("additional capability found:"+k.replace(ADDITIONAL_CAPABILITY, "")+"="+p.getProperty(k));
				String cap=p.getProperty(k);
				
				String cpaKey=k.replace(ADDITIONAL_CAPABILITY, "");
				String[] capVar=cpaKey.split("\\.");
				
				//bonifica eventuali punti prima della capability
				//Original error: Activity name '.posteitaliane.posteapp.appbpol.ui.activity.SplashActivity
				if(capVar[0].equals(APP_ACTIVITY))
				{
					if(cap.startsWith("."))
						cap=cap.substring(1);
				}
				
				switch(capVar[1].toLowerCase().trim())
				{
				case "int":
					caps.setCapability("appium:"+capVar[0], Integer.parseInt(cap.trim()));
					break;
				case "double":
					caps.setCapability("appium:"+capVar[0], Double.parseDouble(cap.trim()));
					break;
				case "boolean":
					caps.setCapability("appium:"+capVar[0], Boolean.parseBoolean(cap.trim()));
					break;
					default:
						caps.setCapability("appium:"+capVar[0], cap.trim());
				}
				
			}
		}

		String serverUrlAsString = extractMandatoryPropertyValue(ANDROID_SERVER_URL, p);

		try {
			AppiumDriver tempDriver = null;

			switch(deviceOs)
			{
			case "ios":
				tempDriver=new IOSDriver<>(new URL(serverUrlAsString), caps);
				break;
			default:
				tempDriver=new AndroidDriver<>(new URL(serverUrlAsString), caps);
			}

			return tempDriver;
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException("Error creating androidDriver. ", e);
		}
	}

	private WebDriver buildFirefoxDriver(Properties p) 
	{
		String chromeDriver = extractMandatoryPropertyValue(FIREFOX_DRIVER, p);
		System.setProperty(FIREFOX_DRIVER, chromeDriver);

		if (HarFileUtils.isHarFile()) {
			// configure Selenium proxy object as a desired capability
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability(CapabilityType.PROXY, HarFileUtils.buildSeleniumProxy(p));

			return new FirefoxDriver(capabilities);

		} else {
			return new FirefoxDriver();
		}
	}

	private WebDriver buildChromeDriver(Properties p) {
		String chromeDriver = extractMandatoryPropertyValue(CHROME_DRIVER, p);
		System.setProperty("webdriver.chrome.driver", chromeDriver);

		if (HarFileUtils.isHarFile()) {
			// configure Selenium proxy object as a desired capability
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability(CapabilityType.PROXY, HarFileUtils.buildSeleniumProxy(p));

			return new ChromeDriver(capabilities);

		} else {
			return new ChromeDriver();
		}
	}

	private WebDriver buildIEDriver(Properties p) {
		String ieDriver = extractMandatoryPropertyValue(IE_DRIVER, p);
		System.setProperty("webdriver.ie.driver", ieDriver);

		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("requireWindowFocus", true);  
		capabilities.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, false);
		capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
		capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		capabilities.setCapability(InternetExplorerDriver.FORCE_CREATE_PROCESS, true);

		if (HarFileUtils.isHarFile()) {
			// configure Selenium proxy object as a desired capability
			capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
			capabilities.setCapability(CapabilityType.PROXY, HarFileUtils.buildSeleniumProxy(p));

			return new InternetExplorerDriver(capabilities);
		} else {
			capabilities.setCapability(InternetExplorerDriver.IE_USE_PRE_PROCESS_PROXY, true);

			return new InternetExplorerDriver(capabilities);

		}
	}

	private WebDriver buildElectronDriver(Properties p) {
		String chromeDriver = extractMandatoryPropertyValue(CHROME_DRIVER, p);
		String electronBinary = extractMandatoryPropertyValue(ELECTRON_BINARY, p);

		// mandatory for selenium
		System.setProperty("webdriver.chrome.driver", chromeDriver);
		Map<String, Object> chromeOptions = new HashMap<String, Object>();
		ChromeOptions options = new ChromeOptions();
		options.setBinary(electronBinary);
		String appArgs = p.getProperty(ELECTRON_APP_ARGS);

		if (StringUtils.isNotBlank(appArgs)) {

			addArgsToElectronApp(options, appArgs);

		}

		DesiredCapabilities capabilities = new DesiredCapabilities();

		if (HarFileUtils.isHarFile()) {
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			capabilities.setBrowserName(DRIVER_TYPE_ELECTRON);
			// configure Selenium proxy object as a desired capability
			capabilities.setCapability(CapabilityType.PROXY, HarFileUtils.buildSeleniumProxy(p));
			return new ChromeDriver(capabilities);

		} else {
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			capabilities.setBrowserName(DRIVER_TYPE_ELECTRON);
			return new ChromeDriver(capabilities);
		}

	}

	/**
	 * Add arguments to the electron application under test in the format
	 * Key=Value.
	 * 
	 */
	private void addArgsToElectronApp(ChromeOptions options, String appArgs) {
		// The value of property electron.app.args must be enclosed in
		// single curly braces.
		// You need to set a comma-separated list of key and value pairs.
		int lastIndex = appArgs.length() - 1;
		char first = appArgs.charAt(0);
		char last = appArgs.charAt(lastIndex);

		if ((first == '{') && (last == '}')) {
			String arguments = appArgs.substring(1, lastIndex);
			String[] args = arguments.split(",");
			List<String> argsList = Arrays.asList(args);

			for (String arg : argsList) {

				// To start Electron application using custom command line
				// arguments
				// you need to specify arguments in the format
				// <Key>=<value>.
				Pattern keyValuePattern = Pattern.compile("(.+?)=(.+?)");
				Matcher matcherKeyValuePattern = keyValuePattern.matcher(arg);

				if (!matcherKeyValuePattern.find()) {
					throw new IllegalArgumentException("The argument " + arg
							+ " of list specified in electron.app.args property must be in the format <Key>=<value>!");
				}

				// The property electron.app.args cannot take the key of
				// each
				// custom argument in camelCase format but with hyphens
				// syntax
				String key = arg.split("=")[0];
				Pattern camelCasePattern = Pattern.compile("[a-z]+[A-Z0-9][a-z0-9]+[A-Za-z0-9]*");
				Matcher matcherCamelCasePattern = camelCasePattern.matcher(key);

				if (matcherCamelCasePattern.find()) {
					throw new IllegalArgumentException("The property electron.app.args cannot take the key of argument "
							+ arg
							+ " in camelCase format but with hyphens syntax! E.g., the key electronPort needs to be changed in electron-port!");
				}

			}
			options.addArguments(argsList);

		} else {

			throw new IllegalArgumentException(
					"The value of property electron.app.args must be enclosed in single curly braces! It must be a comma-separated list of key and value pairs that represents the custom command line arguments of electron application! E.g., {webpack-port=3000,electron-port=8000}");

		}
	}

	/**
	 * Create Selenium RemoteWebDriver to allow UI test execution inside a grid.
	 * 
	 */
	private WebDriver buildRemoteDriver(Properties p) throws MalformedURLException {
		String gridServerUrl = extractMandatoryPropertyValue(GRID_SERVER_URL, p);
		String browserType = extractMandatoryPropertyValue(GRID_BROWSER_TYPE, p);
		String electronAppBinary = p.getProperty(ELECTRON_BINARY);
		String platformType = p.getProperty(GRID_PLATFORM_TYPE);
		String browserVersion = p.getProperty(GRID_BROWSER_VERSION);
		String browserTargetURL = p.getProperty(DRIVER_TARGET_URL);
		String electronAppArgs = p.getProperty(ELECTRON_APP_ARGS);

		DesiredCapabilities capabilities = null;

		if (browserType.equalsIgnoreCase(DRIVER_TYPE_CHROME)) {
			capabilities = setChromeCapabilities(p, electronAppBinary, browserVersion, browserTargetURL,
					electronAppArgs);
		} else if (browserType.equalsIgnoreCase(DRIVER_TYPE_FIREFOX)) {
			capabilities = DesiredCapabilities.firefox();
			if (HarFileUtils.isHarFile()) {
				capabilities.setCapability(CapabilityType.PROXY, HarFileUtils.buildSeleniumProxy(p));
			}
		} else if (browserType.equalsIgnoreCase(DRIVER_TYPE_IEXPLORER)) {
			capabilities = DesiredCapabilities.internetExplorer();
			if (HarFileUtils.isHarFile()) {
				capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
				capabilities.setCapability(CapabilityType.PROXY, HarFileUtils.buildSeleniumProxy(p));
			} else {
				capabilities.setCapability(InternetExplorerDriver.IE_USE_PRE_PROCESS_PROXY, true);
			}
		} else {
			throw new IllegalArgumentException("Unsupported browser type: " + browserType
					+ ". Supported browser types are: chrome, firefox, iexplorer!");
		}

		if (StringUtils.isNotBlank(platformType)) {
			capabilities.setCapability("platform", platformType);
		}

		if (StringUtils.isNotBlank(browserVersion)) {
			capabilities.setVersion(browserVersion);
		}

		return new RemoteWebDriver(new URL(gridServerUrl), capabilities);
	}

	/**
	 * Set capabilities for browser type "Chrome" inside the grid (It includes
	 * also electron applications).
	 * 
	 */
	private DesiredCapabilities setChromeCapabilities(Properties p, String electronAppBinary, String browserVersion,
			String browserTargetURL, String electronAppArgs) {
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		if ((StringUtils.isNotBlank(electronAppBinary)) && (StringUtils.isNotBlank(browserVersion))) {
			throw new IllegalArgumentException("Cannot specify browser version for electron app test inside grid!");
		}
		if ((StringUtils.isNotBlank(electronAppBinary)) && (StringUtils.isNotBlank(browserTargetURL))) {
			throw new IllegalArgumentException("Cannot specify browser target URL for electron app test inside grid!");
		}
		if (StringUtils.isNotBlank(electronAppBinary)) {
			ChromeOptions options = new ChromeOptions();
			options.setBinary(electronAppBinary);
			if (StringUtils.isNotBlank(electronAppArgs)) {

				addArgsToElectronApp(options, electronAppArgs);

			}
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		}
		if (HarFileUtils.isHarFile()) {
			capabilities.setCapability(CapabilityType.PROXY, HarFileUtils.buildSeleniumProxy(p));
		}
		return capabilities;
	}

	/**
	 * Check if the element find by this locator is contained in the DOM
	 * 
	 * @param locator
	 *            The locator used to find the element
	 * @return true if the element is not present, false otherwise
	 */
	public ExpectedCondition<Boolean> elementNotPresent(By locator) {
		return new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
				List<WebElement> findElements = driver.findElements(locator);
				driver.manage().timeouts().implicitlyWait(DEFAULT_IMPLICIT_WAIT_TIMEOUT, TimeUnit.SECONDS);
				return findElements.size() == 0;
			}
		};
	}

	/**
	 * Take a screenshot. The image will be scaled with 1/2 aspect ratio
	 * calculated on current screen dimension. The image will be saved in the
	 * report images directory
	 * 
	 * @param driver
	 * @return the file name created
	 */
	public String takeScreenshot(final WebDriver driver) {
		String fileName = "screenshot_" + System.getProperty(UIUtils.SCENARIO_ID) + ".png";
		String reportDir = System.getProperty(UIUtils.REPORT_IMAGES_DIR);
		String fileNameAbsolutePath = reportDir + File.separatorChar + fileName;
		try {
			File image = null;
			if (!(driver instanceof AndroidDriver)) {
				// BufferedImage scaled = UISupport.captureScreenshoot();
				// ImageIO.write( scaled, "png", new File(fileNameAbsolutePath)
				// );
				image = UISupport.captureScreenshoot(driver);
			} else {
				image = UISupport.captureScreenshoot((AndroidDriver) driver);
			}
			FileUtils.copyFile(image, new File(fileNameAbsolutePath));
		} catch (Exception e) {
			throw new RuntimeException("Error taking screenshot.", e);
		}
		return fileName;
	}

	/**
	 * Take a screenshot. The image will be scaled with 1/2 aspect ratio
	 * calculated on current screen dimension. The image will be saved in the
	 * report images directory
	 * 
	 * @param driver
	 * @deprecated will be removed in next version
	 */
	public void takeScreenshot() {
		try {
			String reportDir = System.getProperty(UIUtils.REPORT_IMAGES_DIR);
			String fileName = "screenshot_" + System.getProperty(UIUtils.SCENARIO_ID) + ".png";
			String fileNameAbsolutePath = reportDir + File.separatorChar + fileName;
			BufferedImage scaled = UISupport.captureScreenshoot();
			ImageIO.write(scaled, "png", new File(fileNameAbsolutePath));
		} catch (Exception e) {
			throw new RuntimeException("Error taking screenshot.", e);
		}
	}

	protected String extractMandatoryPropertyValue(String propertyKey, Properties dictionary) {
		if (dictionary == null) {
			throw new IllegalArgumentException("Property dictionary null!");
		}
		if (StringUtils.isBlank(propertyKey)) {
			throw new IllegalArgumentException("Property key is blank!");
		}
		String property = dictionary.getProperty(propertyKey);
		if (StringUtils.isBlank(property)) {
			throw new IllegalArgumentException("Unable to find property '" + propertyKey + "'. It is mandatory! ");
		}
		return property;

	}

	/**
	 * for internal use only
	 * 
	 * @return
	 */
	public WebDriver getCurrentDriver() {
		return this.currentDriver;
	}

	/**
	 * for internal use only
	 * 
	 * @return
	 */
	public Properties getCurrentProperties() {
		return this.p;
	}

	public Properties getProperties(String driver)
	{
		return this.propertiesBundles.get(driver);
	}

	public DesiredCapabilities getCapabilitiesFor(Properties config) 
	{
		DesiredCapabilities caps=new DesiredCapabilities();

		switch(config.getProperty(UIUtils.DRIVER_TYPE))
		{
		case UIUtils.DRIVER_TYPE_CHROME:
			caps=createChromeCapabilities(config);
			break;
		case UIUtils.DRIVER_TYPE_FIREFOX:
			caps=createFireFoxCapabilities(config);
			break;
		case UIUtils.DRIVER_TYPE_IEXPLORER:
			caps=createIExplorerCapabilities(config);
			break;
		case UIUtils.DRIVER_TYPE_ELECTRON:
			caps=createElectronCapabilities(config);
			break;
		case UIUtils.DRIVER_TYPE_REMOTE:
			caps=createRemoteCapabilities(config);
			break;
		case UIUtils.DRIVER_TYPE_MOBILE:
			caps=createAppiumCapabilities(config);
			break;
		}
		return caps;
	}

	private DesiredCapabilities createAppiumCapabilities(Properties config) 
	{
		// build an AndroidDriver using a capabilities
		DesiredCapabilities caps = new DesiredCapabilities();

		String platformVersion = config.getProperty(ANDROID_PLATFORM_VERSION);
		if(StringUtils.isNotBlank(platformVersion)){

			caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, platformVersion);
		}
		String deviceOs = config.getProperty(DEVICE_OS);
		if(StringUtils.isNotBlank(deviceOs)){

			caps.setCapability("device.os", deviceOs);
		}
		String deviceName = config.getProperty(ANDROID_DEVICE_NAME);
		if(StringUtils.isNotBlank(deviceName)){


			caps.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
		}
		// Move directly into Webview context allowing you to avoid context
		// switch for Hybrid mobile app testing. Default false.
		//				caps.setCapability(MobileCapabilityType.AUTO_WEBVIEW, Boolean.TRUE);

		String appPackage = config.getProperty(ANDROID_APP_PACKAGE);
		if(StringUtils.isNotBlank(appPackage)){


			caps.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, appPackage);
		}
		String appActivity = config.getProperty(ANDROID_APP_ACTIVITY);
		if(StringUtils.isNotBlank(appActivity)){

			caps.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, appActivity);
		}

		String browserName=config.getProperty(BROWSER_NAME);
		if(StringUtils.isNotBlank(browserName)){
			caps.setCapability(MobileCapabilityType.BROWSER_NAME, browserName);
		}

		String fullResetApp= config.getProperty(ANDROID_FULL_RESET_APP);
		if(StringUtils.isNotBlank(fullResetApp)){
			caps.setCapability(MobileCapabilityType.FULL_RESET, fullResetApp);
		}
		// this property is not mandatory
		String chromeDriverMobile = config.getProperty(CHROME_DRIVER);
		if (StringUtils.isNotBlank(chromeDriverMobile)) {
			caps.setCapability(AndroidMobileCapabilityType.CHROMEDRIVER_EXECUTABLE, chromeDriverMobile);
		}
		String newBundleId = config.getProperty(IOS_BUNDLE_ID);
		if (StringUtils.isNotBlank(newBundleId)) {
			caps.setCapability(IOSMobileCapabilityType.BUNDLE_ID, newBundleId);
		}
		String newUdId = config.getProperty(IOS_UDID);
		if (StringUtils.isNotBlank(newUdId)) {
			caps.setCapability(MobileCapabilityType.UDID, newUdId);
		}
		// Not mandatory property to define how long Appium will wait for a new
		// command before ending the session
		String newCommandTimeOut = config.getProperty(ANDROID_NEW_COMMAND_TIMEOUT);
		if (StringUtils.isNotBlank(newCommandTimeOut)) {
			caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, Integer.parseInt(newCommandTimeOut));
		}
		String newAutomationName = config.getProperty(ANDROID_AUTOMATION_NAME);
		if (StringUtils.isNotBlank(newAutomationName)) {
			caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, newAutomationName);
		}
		//				String newNoReset = config.getProperty(ANDROID_NO_RESET);
		//				if(StringUtils.isNotBlank(newNoReset)){
		//					caps.setCapability(MobileCapabilityType.NO_RESET, newNoReset);
		//				}

		String newNoReset = config.getProperty(ANDROID_NO_RESET);
		if(StringUtils.isNotBlank(newNoReset)){
			caps.setCapability(MobileCapabilityType.NO_RESET, newNoReset.equals("true") ? true : false);
		}

		String newAppiumPort = config.getProperty(APPIUM_SYSTEM_PORT);
		if (StringUtils.isNotBlank(newAppiumPort)) {
			caps.setCapability(AndroidMobileCapabilityType.SYSTEM_PORT, newAppiumPort);
		}

		String newmjpegServerPort = config.getProperty(APPIUM_MJPEG_SERVER_PORT);
		if (StringUtils.isNotBlank(newmjpegServerPort)) {
			caps.setCapability("mjpegServerPort", newmjpegServerPort);
		}

		//SOLO IOS
		String startIWDP = config.getProperty(IOS_START_IWDP);
		if (StringUtils.isNotBlank(startIWDP)) {
			caps.setCapability("startIWDP", startIWDP.equals("true"));
		}

		//SOLO IOS
		String fullContextList = config.getProperty(IOS_FULL_CONTEXT_LIST);
		if (StringUtils.isNotBlank(fullContextList)) {
			caps.setCapability("fullContextList", fullContextList.equals("true"));
		}

		//Additional Capabilities
		for(Object c : config.keySet())
		{
			String k=(String) c;
			if(k.startsWith(ADDITIONAL_CAPABILITY))
			{
				System.out.println("additional capability found:"+k.replace(ADDITIONAL_CAPABILITY, "")+"="+config.getProperty(k));
				String cap=config.getProperty(k);
				String cpaKey=k.replace(ADDITIONAL_CAPABILITY, "");
				caps.setCapability(cpaKey, cap);
			}
		}
		return caps;
	}

	private DesiredCapabilities createRemoteCapabilities(Properties config) 
	{
		String gridServerUrl = extractMandatoryPropertyValue(GRID_SERVER_URL, config);
		String browserType = extractMandatoryPropertyValue(GRID_BROWSER_TYPE, config);
		String electronAppBinary = config.getProperty(ELECTRON_BINARY);
		String platformType = config.getProperty(GRID_PLATFORM_TYPE);
		String browserVersion = config.getProperty(GRID_BROWSER_VERSION);
		String browserTargetURL = config.getProperty(DRIVER_TARGET_URL);
		String electronAppArgs = config.getProperty(ELECTRON_APP_ARGS);

		DesiredCapabilities capabilities = null;

		if (browserType.equalsIgnoreCase(DRIVER_TYPE_CHROME)) {
			capabilities = setChromeCapabilities(config, electronAppBinary, browserVersion, browserTargetURL,
					electronAppArgs);
		} else if (browserType.equalsIgnoreCase(DRIVER_TYPE_FIREFOX)) {
			capabilities = DesiredCapabilities.firefox();
			if (HarFileUtils.isHarFile()) {
				capabilities.setCapability(CapabilityType.PROXY, HarFileUtils.buildSeleniumProxy(config));
			}
		} else if (browserType.equalsIgnoreCase(DRIVER_TYPE_IEXPLORER)) {
			capabilities = DesiredCapabilities.internetExplorer();
			if (HarFileUtils.isHarFile()) {
				capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
				capabilities.setCapability(CapabilityType.PROXY, HarFileUtils.buildSeleniumProxy(p));
			} else {
				capabilities.setCapability(InternetExplorerDriver.IE_USE_PRE_PROCESS_PROXY, true);
			}
		} else {
			throw new IllegalArgumentException("Unsupported browser type: " + browserType
					+ ". Supported browser types are: chrome, firefox, iexplorer!");
		}

		if (StringUtils.isNotBlank(platformType)) {
			capabilities.setCapability("platform", platformType);
		}

		if (StringUtils.isNotBlank(browserVersion)) {
			capabilities.setVersion(browserVersion);
		}

		return capabilities;
	}

	private DesiredCapabilities createElectronCapabilities(Properties config) {
		String chromeDriver = extractMandatoryPropertyValue(CHROME_DRIVER, config);
		String electronBinary = extractMandatoryPropertyValue(ELECTRON_BINARY, config);

		// mandatory for selenium
		System.setProperty("webdriver.chrome.driver", chromeDriver);
		Map<String, Object> chromeOptions = new HashMap<String, Object>();
		ChromeOptions options = new ChromeOptions();
		options.setBinary(electronBinary);
		String appArgs = config.getProperty(ELECTRON_APP_ARGS);

		if (StringUtils.isNotBlank(appArgs)) {

			addArgsToElectronApp(options, appArgs);

		}

		DesiredCapabilities capabilities = new DesiredCapabilities();

		if (HarFileUtils.isHarFile()) {
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			capabilities.setBrowserName(DRIVER_TYPE_ELECTRON);
			// configure Selenium proxy object as a desired capability
			capabilities.setCapability(CapabilityType.PROXY, HarFileUtils.buildSeleniumProxy(config));


		} else {
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			capabilities.setBrowserName(DRIVER_TYPE_ELECTRON);

		}

		return capabilities;
	}

	private DesiredCapabilities createIExplorerCapabilities(Properties config) {
		String ieDriver = extractMandatoryPropertyValue(IE_DRIVER,config);
		System.setProperty("webdriver.ie.driver", ieDriver);

		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("requireWindowFocus", true);  
		capabilities.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, false);
		capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
		capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		capabilities.setCapability(InternetExplorerDriver.FORCE_CREATE_PROCESS, true);

		if (HarFileUtils.isHarFile()) {
			// configure Selenium proxy object as a desired capability
			capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
			capabilities.setCapability(CapabilityType.PROXY, HarFileUtils.buildSeleniumProxy(config));
		} else {
			capabilities.setCapability(InternetExplorerDriver.IE_USE_PRE_PROCESS_PROXY, true);
		}

		return capabilities;

	}

	private DesiredCapabilities createFireFoxCapabilities(Properties config) {
		String cd = extractMandatoryPropertyValue(FIREFOX_DRIVER, config);
		System.setProperty(FIREFOX_DRIVER, cd);

		DesiredCapabilities caps=new DesiredCapabilities();
		if (config.getProperty(HarFileUtils.HAR_FILE_ENABLED) != null && 
				StringUtils.isNotBlank(config.getProperty(HarFileUtils.HAR_FILE_ENABLED))) {
			caps.setCapability(CapabilityType.PROXY, HarFileUtils.buildSeleniumProxy(config));
		}
		return caps;
	}

	private DesiredCapabilities createChromeCapabilities(Properties config) 
	{
		String chromeDriver = extractMandatoryPropertyValue(CHROME_DRIVER,config);
		System.setProperty("webdriver.chrome.driver", chromeDriver);
		DesiredCapabilities caps=new DesiredCapabilities();
		if (config.getProperty(HarFileUtils.HAR_FILE_ENABLED) != null && 
				StringUtils.isNotBlank(config.getProperty(HarFileUtils.HAR_FILE_ENABLED))) {
			caps.setCapability(CapabilityType.PROXY, HarFileUtils.buildSeleniumProxy(config));
		}
		return caps;
	}

	public void setCurrentProperties(Properties p2) {
		p=p2;
	}

	public void setDriver(WebDriver t) 
	{
		currentDriver=t;
	}

}
