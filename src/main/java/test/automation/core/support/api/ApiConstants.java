package test.automation.core.support.api;

public final class ApiConstants {

	public final static String DEFAULT_API_PROPERTIES_BUNDLE_KEY = "default";

	public final static String BASE_URI = "apiserver.uri";
	public final static String AUTH_BASIC_USERNAME = "auth.basic.username";
	public final static String AUTH_BASIC_PASSWORD = "auth.basic.password";
	public final static String AUTH_PREEMPTIVE = "auth.basic.preemptive";

	public final static String HTTP_AUTH_ENCRYPTED = "http.auth.encrypted";
	public final static String HTTP_BODY_ENCRYPTED = "http.body.encrypted";

	public final static String SECURITY_PROVIDER_CLASS = "security.provider.class";
	public final static String SECURITY_PROVIDER_PROPERTIES_PATH = "security.provider.properties.path";
	public final static String ENCRYPTION_MODE = "encryption.mode";

	public final static String AUTH_OAM_ENABLED = "auth.oam.enabled";
	public final static String OAUTH2_ENABLED = "oauth2.enabled";
	public final static String OAM_PROPERTIES_PATH = "oam.properties.path";
	public final static String OAUTH2_PROPERTIES_PATH = "oauth2.properties.path";
	public final static String IDENTITY_SENDER = "identity.sender";

}
