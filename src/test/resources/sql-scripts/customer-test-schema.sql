CREATE TABLE customer (
  id   VARCHAR(10) PRIMARY KEY,
  companyCode VARCHAR(10),
  companyName VARCHAR(30),
  trustName VARCHAR(30)
);