CREATE TABLE product (
  id   VARCHAR(10) PRIMARY KEY,
  description VARCHAR(30),
  price  INT
);