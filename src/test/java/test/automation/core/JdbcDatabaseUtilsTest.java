package test.automation.core;

import static org.junit.Assert.assertNotNull;
import static test.automation.core.DatabaseUtils.jdbc;

import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

public class JdbcDatabaseUtilsTest {

	@Before
	public void setup() {

		System.setProperty("confPath", "src/test/resources");
	}

	@Test
	public void testGetDatabaseTemplate() {
		assertNotNull(jdbc().template());

	}

	@Test
	public void testGetRequestTemplateByserverId() {
		assertNotNull(jdbc().template("dbserver"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetRequestTemplateByserverIdKO() {
		assertNotNull(jdbc().template("database2"));
	}

	@Test
	public void testGetRequestTemplateBasicAuthenticationConfigured() {
		JdbcTemplate jdbcTemplate = jdbc().template();
		assertNotNull(jdbcTemplate);
	}
}
