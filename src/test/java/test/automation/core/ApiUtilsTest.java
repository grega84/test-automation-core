package test.automation.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static test.automation.core.ApiUtils.api;

import org.junit.Before;
import org.junit.Test;

import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.specification.FilterableRequestSpecification;
import io.restassured.specification.RequestSpecification;


public class ApiUtilsTest {

	@Before
	public void setup(){
		
		System.setProperty("confPath", "src/test/resources");
	}
	
	@Test
	public void testGetRequestTemplate() {
		//assertNotNull(ApiUtils.getInstance().getRequestTemplate());
		assertNotNull(api().template());
	}

	@Test
	public void testGetRequestTemplateByserverId() {
		//assertNotNull(ApiUtils.getInstance().getRequestTemplate("server1"));
		assertNotNull(api().template("server1"));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testGetRequestTemplateByserverIdKO() {
		//assertNotNull(ApiUtils.getInstance().getRequestTemplate("server2"));
		assertNotNull(api().template("server2"));
	}
	
	@Test
	public void testGetRequestTemplateBasicAuthenticationConfigured() {
		RequestSpecification requestSpecification = api().template("server1");
		assertNotNull(requestSpecification.authentication());
	}
	

}