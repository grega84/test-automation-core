package test.automation.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static test.automation.core.MessageUtils.msg;

import org.apache.activemq.broker.BrokerService;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class MessageUtilTest {

	
	public static BrokerService broker;
	
	@BeforeClass
	public static void setup(){
		
		System.setProperty("confPath", "src/test/resources");
		
		broker = new BrokerService();
		 
		// configure the broker
		try {
			broker.addConnector("tcp://localhost:61617");
			
			broker.start();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Test
	public void testGetTemplate() {
		
		assertNotNull(msg().template());
		
	}
	
	@Test
	public void testSendReceiveTextMessage() {
		
		msg().template().convertAndSend("example.MyQueue", "TEST QUEUE MESSAGE");
		
		Object stringMessage = msg().template().receiveAndConvert("example.MyQueue");
		assertNotNull(stringMessage);
		
		String message = (String)stringMessage;
		assertEquals("TEST QUEUE MESSAGE", message);
			
	}
    
	
	@AfterClass
	public static void after(){
		try {
			broker.stop();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}