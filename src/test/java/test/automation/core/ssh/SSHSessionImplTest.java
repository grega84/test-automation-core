package test.automation.core.ssh;

import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.hamcrest.Matchers;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.test.automation.mock.FakeCommandFactory;
import com.test.automation.mock.SSHServerMock;

import test.automation.core.properties.FileMultiPropertiesLoaderImpl;
import test.automation.core.properties.MultiPropertiesLoader;
import test.automation.core.ssh.SSHRuntimeException;
import test.automation.core.ssh.SSHSession;
import test.automation.core.ssh.SSHSessionImpl;

public class SSHSessionImplTest {
	private static Properties properties = new Properties();
	private static final String USERNAME = "test";
	private static final String PASSWORD = "test";
	private static final Integer PORT = 1300;
	private static SSHServerMock server;

	@BeforeClass
	public static void setup() throws IOException {
		System.setProperty("confPath", "src/test/resources");
		MultiPropertiesLoader multiPropertiesLoader = new FileMultiPropertiesLoaderImpl(
				(file) -> file.getName().split("-")[0]);
		Map<String, Properties> propertiesBundles = multiPropertiesLoader
				.loadAndCheck((d, name) -> name.endsWith("-ssh.properties"), "ssh.host");
		properties = propertiesBundles.get("default");
		server = new SSHServerMock(PORT, PASSWORD, USERNAME);
		FakeCommandFactory fakeCommandFactory = new FakeCommandFactory();
		server.setCommandFactory(fakeCommandFactory);
		try {
			server.start();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void sshSessionTest() {
		SSHSession session = null;
		session = new SSHSessionImpl(properties);
		assertThat(session, Matchers.notNullValue());
	}

	@Test
	public void sendCommandTest() {
		SSHSession session = new SSHSessionImpl(properties);
		assertThat(session.exec("dir"), Matchers.equalTo(0));
	}

	@Test
	public void sendCommandWithTimeOutTest() {
		SSHSession session = new SSHSessionImpl(properties);
		assertThat(session.exec(3000, "dir"), Matchers.equalTo(0));
	}

	@Test(expected = SSHRuntimeException.class)
	public void sendCommandWithLowTimeOutFailedTest() {
		SSHSession session = new SSHSessionImpl(properties);
		assertThat(session.exec(1, "dir"), Matchers.equalTo(0));
	}

	@Test
	public void sendCommandWithInfiniteTimeOutTest() {
		SSHSession session = new SSHSessionImpl(properties);
		assertThat(session.exec(0, "dir"), Matchers.equalTo(0));
	}

	@Test
	public void sendCommandFailedTest() {
		SSHSession session = new SSHSessionImpl(properties);
		assertThat(session.exec("failure"), Matchers.equalTo(1));
	}

	@Test
	public void sendMultipleCommandTest() {
		SSHSession session = new SSHSessionImpl(properties);
		assertThat(session.exec("dir", "cd"), Matchers.equalTo(0));
	}

	@Test
	public void sendMultipleCommandFailedTest() {
		SSHSession session = new SSHSessionImpl(properties);
		assertThat(session.exec("cd", "NotExistigCmd", "dir"), Matchers.equalTo(1));
	}

	@Test
	public void sendAsyncCommand() throws InterruptedException, ExecutionException, TimeoutException {
		SSHSession session = new SSHSessionImpl(properties);
		Integer result = session.execAsync("dir").get(3000, TimeUnit.MILLISECONDS);
		assertThat(result, Matchers.equalTo(0));
	}

	@Test
	public void sendAsyncCommandFailure() throws InterruptedException, ExecutionException, TimeoutException {
		SSHSession session = new SSHSessionImpl(properties);
		Integer result = session.execAsync("notFound").get(3000, TimeUnit.MILLISECONDS);
		assertThat(result, Matchers.equalTo(1));
	}

	@AfterClass
	public static void cleanup() throws InterruptedException {
		try {
			server.stop();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
