package test.automation.core.sikuli;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Iterator;

import org.sikuli.script.App;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Match;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;

import net.sourceforge.tess4j.Tesseract;

public class SikuliTest {

	public static void main(String[] args) 
	{
		org.sikuli.basics.Settings.OcrTextRead=true;
		org.sikuli.basics.Settings.OcrTextSearch=true;
		
		Screen s=Screen.getPrimaryScreen();
		Tesseract t=new Tesseract();
		t.setDatapath("C:\\Program Files (x86)\\Tesseract-OCR\\tessdata");
		t.setLanguage("ita");
		//Pattern p=new Pattern(SikuliTest.class.getClassLoader().getResource("start.png").getFile());
		try {
			App a=new App("notepad");
			a.open();
			a.focus();
			Thread.sleep(2000);
			Region r=a.window();
			r.type("prova prova\r\n");
			Thread.sleep(2000);
			//System.out.println(r.text());
			Iterator<Match> i=r.findAllText("prova");
			System.out.println("find prova");
			
			while(i.hasNext())
			{
				Match m=i.next();
				System.out.println(m);
				m.highlight(2);
				System.out.println(m.text());
				m.x-=10;
				m.w+=10;
				String ff=m.saveScreenCapture();
				System.out.println(ff);
				File f=new File(ff);
				String ocr=t.doOCR(f);
				System.out.println("ocr:"+ocr);
			}
			
			System.out.println("ocr:"+t.doOCR(new File("C:\\Users\\Francesco\\Desktop\\Cattura.PNG")));
			
			a.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
