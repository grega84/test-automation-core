package test.automation.core.commandprompt;

import test.automation.core.cmd.CommandArgs;
import test.automation.core.cmd.CommandPrompt;
import test.automation.core.cmd.CommandPromptJob;

public class Test 
{

	public static void main(String[] args) 
	{
		CommandPrompt cmd=new CommandPrompt("C:\\Users\\Francesco\\.katalon\\tools\\android_sdk\\platform-tools\\adb.exe");
		CommandArgs a=new CommandArgs();
		
		a.addArgument("devices");
		
		CommandPromptJob exec=cmd.create(a);
		exec.run();
		System.out.println(exec.getOutput());
		System.out.println(exec.getState());
		
	}

}
