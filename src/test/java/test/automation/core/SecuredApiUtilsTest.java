package test.automation.core;

import org.easymock.EasyMockSupport;

public class SecuredApiUtilsTest extends EasyMockSupport {
/**
	@Rule
	public EasyMockRule rule = new EasyMockRule(this);

	@Mock
	public EncryptionManagerFactory factory = null;

	@Mock
	public EncryptionManager em = null;

	@Before
	public void setup() {
		System.setProperty("confPath", "src/test/resources");

	}
	
	
	@Test
	public void testReadConfiguration(){
		
		EncryptionManagerBuilder builder = new EncryptionManagerBuilder(securityFactoryClassName, encryptionManagerPropertyFilePath);
	}

	@Test
	public void testAuthEncryption() {
		try {
			// mocks recording
			expect(em.encrypt("NEW_USER:changeme1")).andReturn("abc234def");
			expect(factory.build()).andReturn(em);
			replayAll();
			String credentialsEncoded = Base64.getEncoder().encodeToString("abc234def".getBytes("utf-8"));

			setFinalStatic(ApiUtils.class.getDeclaredField("emf"), factory);

			RequestSpecification request = api().template("authsecured");
			// verify calling order
			verifyAll();

			// verify header expected
			Headers headers = (Headers) Whitebox.getInternalState(request, "requestHeaders");
			Stream<Header> authHeaderAsStream = headers.asList().stream()
					.filter(s -> s.getName().equals("Authorization"));
			Assert.assertEquals("Authorization header size: ", 1, authHeaderAsStream.count()); // just
																								// one
																								// auth
																								// header
			Header header = headers.asList().stream().findFirst().get();

			Assert.assertEquals("Authorization", header.getName());
			String headerValue = "OneLeo-BasicSec " + credentialsEncoded + ", algorithm=\"AES\"";
			Assert.assertEquals(headerValue, header.getValue());

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}

	static void setFinalStatic(Field field, Object newValue) throws Exception {
		field.setAccessible(true);
		// remove final modifier from field
		Field modifiersField = Field.class.getDeclaredField("modifiers");
		modifiersField.setAccessible(true);
		modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
		field.set(null, newValue);
	}
**/
}
