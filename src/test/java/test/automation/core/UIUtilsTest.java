package test.automation.core;

import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import test.automation.core.TestConfigurationException;
import test.automation.core.UIUtils;

public class UIUtilsTest {

	@Before
	public void setup() {
		System.setProperty("confPath", "src/test/resources");
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testUnknowDriver() {
		UIUtils.ui().driver("test1");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testElectronMissingProperties() {
		UIUtils.ui().driver("electron");
	}

	@Test
	public void testExtractPropertiesFromDictionary() {
		Properties p = new Properties();
		p.setProperty("driver.type", "mobile");
		String result = UIUtils.ui().extractMandatoryPropertyValue("driver.type", p);
		Assert.assertTrue(StringUtils.isNotBlank(result));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testExtractPropertiesFromDictionaryWithNotExistentProp() {
		Properties p = new Properties();
		String result = UIUtils.ui().extractMandatoryPropertyValue("driver.type", p);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testExtractPropertiesFromDictionaryWithNullProp() {
		Properties p = null;
		String result = UIUtils.ui().extractMandatoryPropertyValue("driver.type", p);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testExtractPropertiesFromDictionaryWithNullDictionary() {
		Properties p = new Properties();
		String result = UIUtils.ui().extractMandatoryPropertyValue("", p);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRemoteDriverMissingProperties() {
		UIUtils.ui().driver("remote");

	}

	@Test(expected = IllegalArgumentException.class)
	public void testRemoteDriverIncorrectProperties() {
		UIUtils.ui().driver("remote2");

	}

	@Test(expected = IllegalArgumentException.class)
	public void testRemoteDriverIncorrectBrowser() {
		UIUtils.ui().driver("remote3");

	}

	@Test(expected = IllegalArgumentException.class)
	public void testRemoteDriverUnsupportedProperties() {
		UIUtils.ui().driver("remote4");

	}

	@Test(expected = IllegalArgumentException.class)
	public void testCamelCaseFormatElectronArguments() {
		UIUtils.ui().driver("electronargs");

	}

	@Test(expected = IllegalArgumentException.class)
	public void testIncorrectFormatElectronArguments() {
		UIUtils.ui().driver("electronargs1");

	}

	@Test(expected = IllegalArgumentException.class)
	public void testIncorrectBracesElectronArguments() {
		UIUtils.ui().driver("electronargs2");

	}

	@Test(expected = TestConfigurationException.class)
	public void testRemoteServerMalformedURL() {
		UIUtils.ui().driver("remote5");

	}

}
