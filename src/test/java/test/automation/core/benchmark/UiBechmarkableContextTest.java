package test.automation.core.benchmark;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.CoreMatchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;

import test.automation.core.benchmark.BenchmarkableAnnotationObject;
import test.automation.core.benchmark.BenchmarkableContext;
import test.automation.core.benchmark.UiBenchmarkableContext;

public class UiBechmarkableContextTest {
	
	@Rule
	public ErrorCollector collector = new ErrorCollector();
	
	@Test
	public void testUiBenchmarkableContextWithFullAnnotation(){
		String annotation = "@benchmarkable(name=\"testCreatePractice\")";
		List<String> annotations = new ArrayList<>();
		annotations.add(annotation);
		BenchmarkableContext ctx = new UiBenchmarkableContext(annotations,null);
		List<BenchmarkableAnnotationObject> objectList = ctx.getBenchmarkableObjectList();
		
		for(BenchmarkableAnnotationObject obj : objectList ) {
		collector.checkThat(obj, is(notNullValue()));
		collector.checkThat(obj.getName(), CoreMatchers.is("testCreatePractice"));
		
		}
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testUiBenchmarkableContextExpectedExceptionMoreAnnotations(){
		String annotation1 = "@benchmarkable(name=\"testCreatePractice\")";
		String annotation2 = "@benchmarkable(name=\"testReadPractice\")";
		String annotation3 = "@benchmarkable(name=\"testReadPractice2\")";
		List<String> annotations = new ArrayList<>();
		annotations.add(annotation1);
		annotations.add(annotation2);
		annotations.add(annotation3);
		
		BenchmarkableContext ctx = new UiBenchmarkableContext(annotations,null);
		        
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testUiBenchmarkableContextExpectedExceptionMissedName(){
		String annotation = "@benchmarkable(name=\"\")";
		List<String> annotations = new ArrayList<>();
		annotations.add(annotation);
		
		BenchmarkableContext ctx = new UiBenchmarkableContext(annotations,null);
	
	}
	
}
