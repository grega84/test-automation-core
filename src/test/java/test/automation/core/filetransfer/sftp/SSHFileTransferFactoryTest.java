package test.automation.core.filetransfer.sftp;

import static org.junit.Assert.assertNotNull;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.test.automation.mock.SSHServerMock;

import test.automation.core.filetransfer.sftp.SSHFileTransferFactory;
import test.automation.core.ssh.SSHIllegalArgumentException;

public class SSHFileTransferFactoryTest {
	private static final String USERNAME = "testSftp";
	private static final String PASSWORD = "testSftp";
	private static final Integer PORT = 1301;
	private static SSHServerMock server;

	@BeforeClass
	public static void setup() {
		System.setProperty("confPath", "src/test/resources");
		server = new SSHServerMock(PORT, PASSWORD, USERNAME);
		try {
			server.start();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void sshFileTransferFactoryTest() {
		assertNotNull(SSHFileTransferFactory.getInstance());
	}

	@Test
	public void getSSHFileTransferDefaultTest() {
		assertNotNull(SSHFileTransferFactory.getInstance().template());
	}

	@Test
	public void getSSHFileTransferTest() {
		assertNotNull(SSHFileTransferFactory.getInstance().template("sftpserver"));
	}

	@Test(expected = SSHIllegalArgumentException.class)
	public void getSSHFileTransferTestNotExistingTemplate() {
		SSHFileTransferFactory.getInstance().template("sftp");
	}

	@AfterClass
	public static void cleanup() throws InterruptedException {
		try {
			server.stop();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
