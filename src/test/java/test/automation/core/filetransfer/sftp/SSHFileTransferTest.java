package test.automation.core.filetransfer.sftp;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.test.automation.mock.SSHServerMock;

import test.automation.core.filetransfer.sftp.SSHFileTransfer;

public class SSHFileTransferTest {
	private static final String USERNAME = "test";
	private static final String PASSWORD = "test";
	private static final String HOST = "127.0.0.1";
	private static final Integer PORT = 1301;
	private static final String FILE_NAME_ORIGIN = "file_origin.txt";
	private static final String FILE_NAME_DL = "file_downloaded.txt";
	private static final String FILE_NAME_UL = "file_toUpload.txt";
	private static final String ROOT_DIR = "sftproot";
	private static final String DOWNLOAD_DIR = "download";
	private static final String UPLOAD_DIR = "upload";
	private static final String TARGET_DIR = "target";
	private static final String SOURCE_DIR = "source";
	private static final String SAMPLE_TEXT = "Test SFTP";
	private SSHFileTransfer sshFileTransfer;
	@ClassRule
	public static TemporaryFolder tempFolder = new TemporaryFolder();
	private static File sftpRootFolder;
	private static File download;
	private static File upload;
	private static SSHServerMock server;

	@BeforeClass
	public static void setup() {
		/* create DTS virtual file system */
		try {
			sftpRootFolder = tempFolder.newFolder(ROOT_DIR);
			download = new File(sftpRootFolder.getAbsoluteFile() + File.separator + DOWNLOAD_DIR);
			download.mkdir();
			FileUtils.write(new File(download, FILE_NAME_ORIGIN), SAMPLE_TEXT, StandardCharsets.UTF_8);
			upload = new File(sftpRootFolder.getAbsoluteFile() + File.separator + UPLOAD_DIR);
			upload.mkdir();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		/* setup server */
		server = new SSHServerMock(PORT, PASSWORD, USERNAME);
		server.setVirtualFileSystem(sftpRootFolder.toPath());
		try {
			server.start();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private Properties createProperties() {
		Properties properties = new Properties();
		properties.put("ssh.host", HOST);
		properties.put("ssh.port", PORT.toString());
		properties.put("ssh.username", USERNAME);
		properties.put("ssh.password", PASSWORD);
		properties.put("StrictHostKeyChecking", "no");
		properties.put("PreferredAuthentications", "publickey,keyboard-interactive,password");
		return properties;
	}

	@Test
	public void fileTransferDownloadTest() throws IOException {
		File targetFolder = tempFolder.newFolder(TARGET_DIR);
		FileUtils.write(new File(targetFolder, FILE_NAME_DL), StringUtils.EMPTY, StandardCharsets.UTF_8);
		Properties properties = createProperties();
		sshFileTransfer = new SSHFileTransfer(properties);
		String fromPath = DOWNLOAD_DIR + File.separator + FILE_NAME_ORIGIN;
		String toPath = targetFolder.getCanonicalPath() + File.separator + FILE_NAME_DL;
		sshFileTransfer.download(fromPath, toPath);
		File file = new File(toPath);
		assertTrue("File not downloaded ", file.exists());
	}

	@Test
	public void fileTransferUploadTest() throws IOException {
		File sourceFolder = tempFolder.newFolder(SOURCE_DIR);
		FileUtils.write(new File(sourceFolder, FILE_NAME_UL), SAMPLE_TEXT, StandardCharsets.UTF_8);
		Properties properties = createProperties();
		sshFileTransfer = new SSHFileTransfer(properties);
		String fromPath = sourceFolder.getCanonicalPath() + File.separator + FILE_NAME_UL;
		sshFileTransfer.upload(fromPath, UPLOAD_DIR);
		File file = new File(upload.getCanonicalPath() + File.separator + FILE_NAME_UL);
		assertTrue("File not uploaded", file.exists());
	}

	@AfterClass
	public static void tearDown() {
		try {
			server.stop();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}