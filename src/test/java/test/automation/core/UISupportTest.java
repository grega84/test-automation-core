package test.automation.core;

import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import test.automation.core.UISupport;

public class UISupportTest {
	
	@Test(expected = IllegalArgumentException.class)
	public void testTakeSceenshotWithNullDriver(){
		WebDriver driver = null;
		UISupport.captureScreenshoot(driver);
	}
	
	@Test
	public void testTakeScreenShot(){
		RemoteWebDriver driver = Mockito.mock(RemoteWebDriver.class);
		File tempScreenshotFile = null;
		try {
			tempScreenshotFile = File.createTempFile("screenshot", ".jpg");
		} catch (IOException e) {
			Assert.fail(e.getMessage());
		}
		when (driver.getScreenshotAs(OutputType.FILE)).thenReturn(tempScreenshotFile);
		UISupport.captureScreenshoot(driver);
		Assert.assertNotNull(driver.getScreenshotAs(OutputType.FILE));
	}

}
