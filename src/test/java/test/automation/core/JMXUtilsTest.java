package test.automation.core;

import static org.junit.Assert.assertNotNull;
import static test.automation.core.JMXUtils.jmx;

import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.management.InstanceNotFoundException;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.remote.JMXServiceURL;

import org.eclipse.jetty.jmx.ConnectorServer;
import org.eclipse.jetty.jmx.MBeanContainer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.util.log.Log;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.test.automation.mock.JmxCalculator;
import com.test.automation.mock.JmxCalculatorMBean;

import test.automation.core.JMXUtils;

public class JMXUtilsTest {
	private static Server server;

	@BeforeClass
	public static void setup() {
		System.setProperty("confPath", "src/test/resources");
		server = new Server();
		// Setup JMX
		MBeanContainer mbeanContainer = new MBeanContainer(ManagementFactory.getPlatformMBeanServer());
		server.addBean(mbeanContainer);
		// Setup ConnectorServer
		JMXServiceURL jmxURL;
		ConnectorServer jmxServer = null;
		Map<String, String> env = new HashMap<String, String>();
		String confPath = System.getProperty("confPath");
		env.put("jmx.remote.x.password.file", confPath + "/jmxremote.password");
		env.put("jmx.remote.x.access.file", confPath + "/jmxremote.access");
		try {
			jmxURL = new JMXServiceURL("service:jmx:rmi://localhost:1100/jndi/rmi://localhost:1099/jmxrmi");
			jmxServer = new ConnectorServer(jmxURL, env, "org.eclipse.jetty.jmx:name=rmiconnectorserver");
			server.addBean(jmxServer);
			// Adding Log
			server.addBean(Log.getLog());
			// Starting the Server
			server.start();
			// Adding MBean Calculator
			MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
			ObjectName name = new ObjectName("domain:name=calculator");
			JmxCalculatorMBean mbean = new JmxCalculator();
			mbs.registerMBean(mbean, name);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void testGetTemplate() {
		assertNotNull(jmx().template());
	}

	@Test
	public void testGetTemplateByserverId() {
		assertNotNull(jmx().template("server"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetTemplateByserverIdKO() {
		assertNotNull(jmx().template("server2"));
	}

	@Test
	public void testGetTemplateBasicAuthenticationConfigured() {
		JMXUtils jConsole = jmx().template();
		assertNotNull(jConsole);
	}

	@Test
	public void testInvokeOperation() throws InstanceNotFoundException {
		String mbeanName = "domain:name=calculator";
		String nameOperation = "sum";
		List<Object> params = new ArrayList<Object>();
		params.add(3);
		params.add(4);
		int result = (int) jmx().template().invokeOperation(mbeanName, nameOperation, params);
		Assert.assertEquals(7, result);
	}

	@AfterClass
	public static void teardown() {
		try {
			server.stop();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
