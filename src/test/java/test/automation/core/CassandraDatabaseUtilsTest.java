package test.automation.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static test.automation.core.DatabaseUtils.cassandra;

import org.cassandraunit.CQLDataLoader;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

public class CassandraDatabaseUtilsTest {

	private static Session session;
	private static ResultSet rs;
	private static Row row;
	private static String CASSANDRA_SERVER_CONFIG = "cassandra-config.yaml";
	private static String CREATE_KEYSPACE_CQL = "cassandra/create-keyspace.cql";
	private static String CREATE_TABLE_CQL = "cassandra/create-table.cql";
	private static String INSERT_CQL = "INSERT INTO customers_by_code (customer_code, companycode, companyname, trustname) VALUES('000777','22', 'CASSANDRA TEST', 'Cassandra Global Test');";
	private static String SELECT_CQL = "select trustname from customers_by_code where customer_code = '000777'";
	private static String UPDATE_CQL = "UPDATE customers_by_code SET trustname='Cassandra Global' WHERE customer_code = '000777'";
	private static String DELETE_CQL = "DELETE FROM customers_by_code WHERE customer_code = '000777'";

	@BeforeClass
	public static void setup() {

		System.setProperty("confPath", "src/test/resources/cassandra");

		try {
			EmbeddedCassandraServerHelper.startEmbeddedCassandra(
					CASSANDRA_SERVER_CONFIG, 300000);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new RuntimeException(e);
		}

		CQLDataLoader cqlDataLoader = new CQLDataLoader(
				EmbeddedCassandraServerHelper.getSession());
		cqlDataLoader.load(new ClassPathCQLDataSet(CREATE_KEYSPACE_CQL));
		cqlDataLoader.load(new ClassPathCQLDataSet(CREATE_TABLE_CQL));

		session = cassandra().template();

	}

	@Test
	public void testGetTemplate() {

		assertNotNull(cassandra().template());

	}

	@Test
	public void testGetTemplateByServerId() {
		assertNotNull(cassandra().template("server"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetTemplateByServerIdKO() {

		assertNotNull(cassandra().template("server2"));

	}

	@Test
	public void testInsertCassandra() {

		session.execute(INSERT_CQL);
		rs = session.execute(SELECT_CQL);
		row = rs.one();
		assertNotNull(row);
		assertEquals("Cassandra Global Test", row.getString("trustname"));

	}

	@Test
	public void testUpdateCassandra() {

		session.execute(UPDATE_CQL);
		rs = session.execute(SELECT_CQL);
		row = rs.one();
		assertNotNull(row);
		assertEquals("Cassandra Global", row.getString("trustname"));

	}

	@Test
	public void testDeleteCassandra() {

		session.execute(DELETE_CQL);
		rs = session.execute(SELECT_CQL);
		row = rs.one();
		assertNull(row);

	}

	@AfterClass
	public static void tearDown() {

		if (session != null) {
			session.close();
		}

		EmbeddedCassandraServerHelper.cleanEmbeddedCassandra();
	}

}
