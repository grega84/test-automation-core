package test.automation.core;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static test.automation.core.JanusGraphUtils.janus;

import java.io.File;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.apache.tinkerpop.gremlin.driver.Result;
import org.apache.tinkerpop.gremlin.driver.ResultSet;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import test.automation.core.graph.GremlinServerHelper;
import test.automation.core.graph.YAMLConfiguration;

public class JanusGraphUtilsTest {

	private static GremlinServerHelper gremlinServerHelper = new GremlinServerHelper();

	@BeforeClass
	public static void setup() throws Exception {
		System.setProperty("confPath", "src/test/resources");
		gremlinServerHelper.startServer();
	}

	@Test
	public void testGetTemplateByServerId() {
		assertNotNull(janus().template("serverjanus"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetTemplateByServerIdKO() {

		assertNotNull(janus().template("serverjanus2"));

	}

	@Test
	public void shouldSubmitScript() throws InterruptedException, ExecutionException {
		List<Result> results = janus().template().executeGraph("g.V().valueMap()").all().get();
		assertThat(results, notNullValue());
		assertThat(results, not(empty()));
	}

	@Test
	public void shouldReturnAnElementFromSpecificParam() throws InterruptedException, ExecutionException {
		Map<String, Object> params = new HashMap<>();
		params.put("x", "name");
		ResultSet executeGraphParam = janus().template().executeGraph("g.V().has(\"person\", \"age\", 29).values(x)",
				params);
		final List<Result> resultsParam = executeGraphParam.all().join();
		assertEquals(1, resultsParam.size());
		assertEquals("marko", resultsParam.get(0).getString());
	}

	@Test
	public void shouldReturnAnElementFromSpecificParamAsyncWay() throws InterruptedException, ExecutionException {
		Map<String, Object> params = new HashMap<>();
		params.put("x", "name");
		CompletableFuture<ResultSet> executeGraphAsync = janus().template()
				.executeGraphAsync("g.V().has(\"person\", \"age\", 29).values(x)", params);
		ResultSet resultsAsync = executeGraphAsync.get();
		final List<Result> resultsParamAsync = resultsAsync.all().join();
		assertEquals(1, resultsParamAsync.size());
		assertEquals("marko", resultsParamAsync.get(0).getString());

	}

	@Test
	public void shouldReturnVertexCount() throws Exception {

		GraphTraversal<Vertex, Vertex> property = janus().template().G().addV("Customer").property("Code", "00001");
		Vertex next = property.next();
		assertNotNull(next);
		String code = (String) janus().template().G().V().values("Code").next();

		ResultSet executeGraph = janus().template().executeGraph("g.V().count()");
		final List<Result> results = executeGraph.all().join();
		assertEquals(1, results.size());
		assertEquals("7", results.get(0).getString());

		assertEquals("00001", code);

	}

	@Test
	public void readConfigurationFromPaths() throws Exception {

		final YAMLConfiguration conf = new YAMLConfiguration(
				Paths.get("src", "test", "resources", "graph", "remote-objects.yaml"));

		assertThat(conf, notNullValue());

		final String[] hosts = conf.getStringArray("hosts");
		assertThat(hosts, notNullValue());
		assertThat(hosts.length, equalTo(1));
		assertThat(hosts[0], equalTo("127.0.0.1"));

	}

	@Test
	public void readConfigurationFromClasspath() throws Exception {

		final YAMLConfiguration conf = new YAMLConfiguration(
				getClass().getClassLoader().getResource("graph/remote-objects.yaml"));

		assertThat(conf, notNullValue());

		final String[] hosts = conf.getStringArray("hosts");
		assertThat(hosts, notNullValue());
		assertThat(hosts.length, equalTo(1));
		assertThat(hosts[0], equalTo("127.0.0.1"));

	}

	@Test
	public void readConfigurationFromFileObject() throws Exception {

		File file = new File("src/test/resources/graph/remote-objects.yaml");

		final YAMLConfiguration conf = new YAMLConfiguration(file);

		assertThat(conf, notNullValue());

		final String[] hosts = conf.getStringArray("hosts");
		assertThat(hosts, notNullValue());
		assertThat(hosts.length, equalTo(1));
		assertThat(hosts[0], equalTo("127.0.0.1"));

	}

	@Test
	public void testConfYaml() throws InterruptedException, ExecutionException {

		List<Result> results = janus().template("serverjanus").executeGraph("g.V().valueMap()").all().get();
		assertThat(results, notNullValue());
		assertThat(results, not(empty()));

	}

	@AfterClass
	public static void stopServer() throws Exception {
		janus().template().close();
		gremlinServerHelper.stopServer();

	}

}
