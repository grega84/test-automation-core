package com.test.automation.mock;

import io.confluent.kafka.schemaregistry.client.MockSchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.confluent.kafka.serializers.KafkaAvroSerializer;

import java.util.Map;

public class KafkaAvroSerializerMock extends KafkaAvroSerializer {

	public KafkaAvroSerializerMock() {

	}

	public KafkaAvroSerializerMock(SchemaRegistryClient client) {
		schemaRegistry = new MockSchemaRegistryClient();
	}

	public KafkaAvroSerializerMock(SchemaRegistryClient client,
			Map<String, ?> props) {
		schemaRegistry = new MockSchemaRegistryClient();
		configure(serializerConfig(props));
	}

	@Override
	public void configure(Map<String, ?> configs, boolean isKey) {
		schemaRegistry = new MockSchemaRegistryClient();
		super.configure(configs, isKey);
	}

}
