package com.test.automation.mock;

public class JmxCalculator implements JmxCalculatorMBean {
	/*
	 * @see
	 * com.oneleo.test.automation.core.JmxCalculatorMBean#sum(java.lang.Integer,
	 * java.lang.Integer)
	 */
	@Override
	public Integer sum(Integer oper1, Integer oper2) {
		int sum = oper1 + oper2;
		return sum;
	}

	/*
	 * @see com.oneleo.test.automation.core.JmxCalculatorMBean#diff(java.lang.
	 * Integer, java.lang.Integer)
	 */
	@Override
	public Integer diff(Integer oper1, Integer oper2) {
		int diff = oper1 - oper2;
		return diff;
	}

	/*
	 * @see com.oneleo.test.automation.core.JmxCalculatorMBean#molt(java.lang.
	 * Integer, java.lang.Integer)
	 */
	@Override
	public Integer molt(Integer oper1, Integer oper2) {
		int molt = oper1 * oper2;
		return molt;
	}
}
