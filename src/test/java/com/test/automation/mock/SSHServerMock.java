package com.test.automation.mock;

import java.nio.file.Path;
import java.security.PublicKey;
import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;
import org.apache.sshd.common.NamedFactory;
import org.apache.sshd.common.file.virtualfs.VirtualFileSystemFactory;
import org.apache.sshd.server.Command;
import org.apache.sshd.server.CommandFactory;
import org.apache.sshd.server.SshServer;
import org.apache.sshd.server.auth.password.PasswordAuthenticator;
import org.apache.sshd.server.auth.pubkey.PublickeyAuthenticator;
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider;
import org.apache.sshd.server.scp.ScpCommandFactory;
import org.apache.sshd.server.session.ServerSession;
import org.apache.sshd.server.subsystem.sftp.SftpSubsystemFactory;

public class SSHServerMock {
	private SshServer server;
	private static final String DEFAULT_HOST = "127.0.0.1";
	private static final int DEFAULT_PORT = 22;

	public SSHServerMock(String host, int port, String userName, String psw) {
		server = SshServer.setUpDefaultServer();
		server.setHost(host);
		server.setPort(port);
		server.setPasswordAuthenticator(new PasswordAuthenticator() {
			@Override
			public boolean authenticate(final String username, final String password, final ServerSession session) {
				return StringUtils.equals(username, userName) && StringUtils.equals(password, psw);
			}
		});
		server.setKeyPairProvider(new SimpleGeneratorHostKeyProvider());
		server.setPublickeyAuthenticator(new PublickeyAuthenticator() {
			@Override
			public boolean authenticate(String username, PublicKey key, ServerSession session) {
				return true;
			}
		});
		server.setSubsystemFactories(Arrays.<NamedFactory<Command>> asList(new SftpSubsystemFactory.Builder().build()));
	}

	public SSHServerMock(int port, String userName, String password) {
		this(DEFAULT_HOST, port, userName, password);
	}

	public SSHServerMock(String userName, String password) {
		this(DEFAULT_PORT, userName, password);
	}

	public void start() throws Exception {
		server.start();
	}

	public void stop() throws Exception {
		server.stop();
	}
	
	public boolean isAlive(){
		return server.isOpen();
	}

	public void setCommandFactory(CommandFactory comFactory) {
		ScpCommandFactory factory = new ScpCommandFactory();
		factory.setDelegateCommandFactory(comFactory);
		server.setCommandFactory(factory);
	}

	public void setVirtualFileSystem(Path vfPath) {
		VirtualFileSystemFactory vfSysFactory = new VirtualFileSystemFactory();
		vfSysFactory.setDefaultHomeDir(vfPath);
		server.setFileSystemFactory(vfSysFactory);
	}
}
