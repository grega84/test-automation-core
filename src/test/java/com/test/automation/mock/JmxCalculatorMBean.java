package com.test.automation.mock;

public interface JmxCalculatorMBean {
	Integer sum(Integer oper1, Integer oper2);

	Integer diff(Integer oper1, Integer oper2);

	Integer molt(Integer oper1, Integer oper2);
}